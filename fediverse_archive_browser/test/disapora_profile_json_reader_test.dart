// ignore_for_file: avoid_print

import 'package:flutter_test/flutter_test.dart';
import 'package:fediverse_archive_browser/src/diaspora/services/diaspora_profile_json_reader.dart';
import 'package:fediverse_archive_browser/src/models/timeline_entry.dart';
import 'package:fediverse_archive_browser/src/services/connections_manager.dart';

const jsonPath = '/Users/hankdev/Desktop/diaspora_pretty.json';

void main() {
  test('Diaspora Connections Test', () {
    final reader = DiasporaProfileJsonReader(jsonPath, ConnectionsManager());
    final contacts = reader.readContacts();
    print(contacts.length);
    print(contacts.first);
  });

  test('Diaspora Posts Test', () {
    final reader = DiasporaProfileJsonReader(jsonPath, ConnectionsManager());
    final posts = reader.readPosts();

    print(posts.length);
    print(posts.first);

    final postsWithImage = posts.firstWhere((element) => element.mediaAttachments.isNotEmpty, orElse: ()=>TimelineEntry());
    print(postsWithImage);

    final resharePost = posts.firstWhere((element) => element.externalLink.isNotEmpty, orElse: ()=>TimelineEntry());
    print(resharePost);

  });

}
