// ignore_for_file: avoid_print

import 'package:flutter_test/flutter_test.dart';
import 'package:fediverse_archive_browser/src/utils/word_map_generator.dart';

void main() {
  test('Empty collection stats', () {
    final generator = WordMapGenerator();
    expect(generator.getTopList(10).isEmpty, true);
  });

  test('Simple Sentence', () {
    final generator = WordMapGenerator();
    generator.processEntry('The quick brown fox jumped over the lazy dog.');
    final list = generator.getTopList(10);
    list.forEach(print);
    expect(list.length, equals(8));
    expect(list.firstWhere((e) => e.word == 'the').count, equals(2));
    expect(list.firstWhere((e) => e.word == 'dog').count, equals(1));
  });

  test('Test case-insensitive', () {
    final generator = WordMapGenerator();
    generator.processEntry('The THE tHE THe the');
    final list = generator.getTopList(10);
    list.forEach(print);
    expect(list.length, equals(1));
    expect(list.firstWhere((e) => e.word == 'the').count, equals(5));
  });

  test('Test punctuation filtering', () {
    final generator = WordMapGenerator();
    generator.processEntry('.the. ,the, :the:  ;the; .the. "the" %the% !the!');
    generator.processEntry('@the@ #the# %the% ^the^ &the& *the* (the( )the)');
    generator.processEntry('-the- _the_ {the{ }the} +the+ =the+ /the/ ?the?');
    generator.processEntry('<the< >the> ~the~ `the`[the[ ]the] |the|');
    generator.processEntry("'the'");
    generator.processEntry(r'$the$ \the\');
    final list = generator.getTopList(10);
    list.forEach(print);
    expect(list.length, equals(1));
    expect(list.firstWhere((e) => e.word == 'the').count, equals(34));
  });

  test('Test minimum word size filter', () {
    final generator = WordMapGenerator(minimumWordSize: 4);
    generator.processEntry('A an the test testing');
    final list = generator.getTopList(10);
    list.forEach(print);
    expect(list.length, equals(2));
    expect(
        list.map((e) => e.word).toSet().containsAll(['test', 'testing']), true);
  });
}
