// ignore_for_file: avoid_print

import 'package:flutter_test/flutter_test.dart';
import 'package:fediverse_archive_browser/src/models/model_utils.dart';
import 'package:logging/logging.dart';

void main() {
  final entries = <LogRecord>[];
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((event) {
    print(
        '${event.level.name} - ${event.loggerName} @ ${event.time}: ${event.message}');
    entries.add(event);
  });
  final logger = Logger('AdditionalKeyLoggerTest');

  group('Test logAdditionalKeys', () {
    test('Exact matching sets', () {
      entries.clear();
      final expectedSet = ['Key1', 'Key2', 'Key3'];
      final actualSet = ['Key1', 'Key2', 'Key3'];
      logAdditionalKeys(
          expectedSet, actualSet, logger, Level.SEVERE, 'Unknown key:');
      expect(entries.isEmpty, true);
    });

    test('Expected Set With more', () {
      entries.clear();
      final expectedSet = ['Key1', 'Key2', 'Key3', 'Key4'];
      final actualSet = ['Key1', 'Key2', 'Key3'];
      logAdditionalKeys(
          expectedSet, actualSet, logger, Level.SEVERE, 'Unknown key:');
      expect(entries.isEmpty, true);
    });

    test('Extra keys in actual set', () {
      entries.clear();
      final expectedSet = ['Key1', 'Key2', 'Key3'];
      final actualSet = ['Key1', 'Key2', 'Key3', 'Key4'];
      logAdditionalKeys(
          expectedSet, actualSet, logger, Level.SEVERE, 'Unknown key:');
      expect(entries.isNotEmpty, true);
    });

    test('Empty expected set', () {
      entries.clear();
      final expectedSet = [];
      final actualSet = ['Key1', 'Key2', 'Key3', 'Key4'];
      logAdditionalKeys(
          expectedSet, actualSet, logger, Level.SEVERE, 'Unknown key:');
      expect(entries.isNotEmpty, true);
    });

    test('Empty actual set', () {
      entries.clear();
      final expectedSet = ['Key1', 'Key2', 'Key3'];
      final actualSet = [];
      logAdditionalKeys(
          expectedSet, actualSet, logger, Level.SEVERE, 'Unknown key:');
      expect(entries.isEmpty, true);
    });

    test('Empty sets', () {
      entries.clear();
      final expectedSet = [];
      final actualSet = [];
      logAdditionalKeys(
          expectedSet, actualSet, logger, Level.SEVERE, 'Unknown key:');
      expect(entries.isEmpty, true);
    });
  });
}
