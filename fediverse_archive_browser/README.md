# Fediverse Archive Browser

A Flutter-based cross platform desktop application for browsing 
the archives from Diaspora or Friendica. The Diaspora archive can be exported
directly from your profile on your pod. The Friendica archive can be
generate and update using the [Friendica Archiver](https://gitlab.com/mysocialportal/fediverse-archiving-tools/-/tree/main/friendica_archiver). 
The archive takes the form of a folder with a series of JSON files and
a local image archive. Simply point the application at this folder and
begin usage.

## Installation

To install the archive browser you simply have to download the latest release from the below links for each platform:
* [Windows x64](https://mysocialportal-friendica-archiving.sfo3.digitaloceanspaces.com/browser/releases/1.0.0/friendica_archive_browser_v1.0.0_win_x64.zip)
* [macOS x64](https://mysocialportal-friendica-archiving.sfo3.digitaloceanspaces.com/browser/releases/1.0.0/friendica_archive_browser_v1.0.0_macos_x64.zip)
* [Linux x64](https://mysocialportal-friendica-archiving.sfo3.digitaloceanspaces.com/browser/releases/1.0.0/friendica_archive_browser_v1.0.0_linux_x64.zip)

Then unzip the folder and you are ready to run. On Mac 
and Windows you will get a warning about an "unknown publisher" since this is beta
software that is not installed or signed through the respective app stores. App store
versions will come in the near future.

## Building
In order to build this application you will need to have installed [Flutter](https://flutter.dev). 
Installation instructions for various platforms are [here](https://flutter.dev/docs/get-started/install).
Once you have that installed it is as easy as navigating to the respective directory on the command
line and executing:

On Linux:
```bash
flutter run -d linux
```

On Mac:
```bash
flutter run -d macos
```

On Windows:
```bash
flutter run -d windows
```

Please report any bugs or feature requests [with our issue tracker](https://gitlab.com/mysocialportal/friendica-archiving-tools/-/issues).
