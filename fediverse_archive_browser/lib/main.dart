import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fediverse_archive_browser/src/utils/temp_file_builder.dart';
import 'package:logging/logging.dart';

import 'src/app.dart';
import 'src/settings/settings_controller.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final logPath = await setupLogging();
  Logger.root.info('Starting Fediverse Archive Browser');
  final settingsController = SettingsController(logPath: logPath);
  await settingsController.loadSettings();
  runApp(FediverseArchiveBrowser(settingsController: settingsController));
}

Future<String> setupLogging() async {
  final logFilePath = await getTempFile('fediverse_archive_browser', '.log');
  final logFile = File(logFilePath);
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((event) {
    final logName = event.loggerName.isEmpty ? 'ROOT' : event.loggerName;
    final msg =
        '${event.level.name} - $logName @ ${event.time}: ${event.message}\n';
    final handle = logFile.openSync(mode: FileMode.append);
    handle.writeStringSync(msg);
    handle.closeSync();
  });

  return logFilePath;
}
