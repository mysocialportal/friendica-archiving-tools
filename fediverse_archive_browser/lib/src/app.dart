import 'package:desktop_window/desktop_window.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:fediverse_archive_browser/src/services/archive_service_provider.dart';
import 'package:fediverse_archive_browser/src/themes.dart';
import 'package:fediverse_archive_browser/src/utils/scrolling_behavior.dart';
import 'package:provider/provider.dart';

import 'home.dart';
import 'settings/settings_controller.dart';

/// The Widget that configures your application.
class FediverseArchiveBrowser extends StatelessWidget {
  static const minAppSize = Size(915, 700);

  const FediverseArchiveBrowser({
    Key? key,
    required this.settingsController,
  }) : super(key: key);

  final SettingsController settingsController;

  @override
  Widget build(BuildContext context) {
    DesktopWindow.setMinWindowSize(minAppSize);
    final archiveService = ArchiveServiceProvider(settingsController);
    settingsController.addListener(() {
      archiveService.clearCaches();
    });
    return AnimatedBuilder(
      animation: settingsController,
      builder: (BuildContext context, Widget? child) {
        return MaterialApp(
          restorationScopeId: 'app',
          localizationsDelegates: const [
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          supportedLocales: const [
            Locale('en', ''), // English, no country code
          ],
          onGenerateTitle: (BuildContext context) =>
              AppLocalizations.of(context)!.appTitle,
          theme: FediverseArchiveBrowserTheme.light,
          darkTheme: FediverseArchiveBrowserTheme.dark,
          themeMode: settingsController.themeMode,
          scrollBehavior: AppScrollingBehavior(),
          home: MultiProvider(
            providers: [
              ChangeNotifierProvider(create: (context) => settingsController),
              ChangeNotifierProvider(create: (context) => archiveService),
            ],
            child: Home(
                settingsController: settingsController,
                archiveService: archiveService),
          ),
        );
      },
    );
  }
}
