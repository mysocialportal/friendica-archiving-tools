import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fediverse_archive_browser/src/models/archive_types_enum.dart';
import 'package:fediverse_archive_browser/src/settings/video_player_settings.dart';
import 'package:fediverse_archive_browser/src/utils/temp_file_builder.dart';
import 'package:intl/intl.dart';
import 'package:logging/logging.dart';
import 'package:path_provider/path_provider.dart';
import 'package:result_monad/result_monad.dart';

import 'settings_service.dart';

class SettingsController with ChangeNotifier {
  final String logPath;
  final SettingsService _settingsService;

  SettingsController({required this.logPath})
      : _settingsService = SettingsService();

  Future<void> loadSettings() async {
    _archiveType = await _settingsService.archiveType();
    _themeMode = await _settingsService.themeMode();
    _rootFolder = await _settingsService.rootFolder();
    var canReadRootDir =
        runCatching(() => Result.ok(Directory(_rootFolder).listSync()))
            .isSuccess;
    if (!canReadRootDir) {
      _rootFolder = '';
    }
    _videoPlayerSettingType = await _settingsService.videoPlayerSettingType();
    _videoPlayerCommand = await _settingsService.videoPlayerCommand();
    _dateTimeFormatter = DateFormat('MMMM dd yyyy h:mm a');
    _dateFormatter = DateFormat('MMMM dd yyyy');
    _logLevel = await _settingsService.logLevel();
    _appDataDirectory = await getApplicationSupportDirectory();
    _geoCacheDirectory = await getTileCachedDirectory();
    Logger.root.level = _logLevel;
    notifyListeners();
  }

  late Directory _geoCacheDirectory;

  Directory get geoCacheDirectory => _geoCacheDirectory;

  late Directory _appDataDirectory;

  Directory get appDataDirectory => _appDataDirectory;

  late Level _logLevel;

  Level get logLevel => _logLevel;

  Future<void> updateLogLevel(Level newLevel) async {
    if (newLevel == _logLevel) return;
    _logLevel = newLevel;
    Logger.root.level = _logLevel;
    await _settingsService.updateLevel(newLevel);
    notifyListeners();
  }

  late DateFormat _dateTimeFormatter;

  DateFormat get dateTimeFormatter => _dateTimeFormatter;

  late DateFormat _dateFormatter;

  DateFormat get dateFormatter => _dateFormatter;

  late String _rootFolder;

  String get rootFolder => _rootFolder;

  Future<void> updateRootFolder(String newPath) async {
    if (newPath == _rootFolder) return;
    _rootFolder = newPath;
    notifyListeners();
    await _settingsService.updateRootFolder(newPath);
  }

  late ArchiveType _archiveType;

  ArchiveType get archiveType => _archiveType;

  Future<void> updateArchiveType(ArchiveType newArchiveType) async {
    if (newArchiveType == _archiveType) return;
    _archiveType = newArchiveType;
    notifyListeners();
    await _settingsService.updateArchiveType(newArchiveType);
  }

  late ThemeMode _themeMode;

  ThemeMode get themeMode => _themeMode;

  Future<void> updateThemeMode(ThemeMode? newThemeMode) async {
    if (newThemeMode == null) return;

    // Dot not perform any work if new and old ThemeMode are identical
    if (newThemeMode == _themeMode) return;

    // Otherwise, store the new theme mode in memory
    _themeMode = newThemeMode;

    // Important! Inform listeners a change has occurred.
    notifyListeners();

    // Persist the changes to a local database or the internet using the
    // SettingService.
    await _settingsService.updateThemeMode(newThemeMode);
  }

  late VideoPlayerSettingType _videoPlayerSettingType;

  VideoPlayerSettingType get videoPlayerSettingType => _videoPlayerSettingType;

  Future<void> updateVideoPlayerSettingType(VideoPlayerSettingType type) async {
    if (type == _videoPlayerSettingType) return;
    _videoPlayerSettingType = type;
    if (_videoPlayerSettingType != VideoPlayerSettingType.custom) {
      await _resetVideoPlayerCommand();
    }
    notifyListeners();
    await _settingsService.updateVideoPlayerSettingType(type);
  }

  late String _videoPlayerCommand;

  String get videoPlayerCommand => _videoPlayerCommand;

  Future<void> updateVideoPlayerCommand(String newCommand) async {
    if (newCommand == _videoPlayerCommand) return;
    _videoPlayerCommand = newCommand;
    notifyListeners();
    await _settingsService.updateVideoPlayerCommand(newCommand);
  }

  Future<void> _resetVideoPlayerCommand() async {
    _videoPlayerCommand = _videoPlayerSettingType.toAppPath();
    await _settingsService.updateVideoPlayerCommand(_videoPlayerCommand);
  }
}
