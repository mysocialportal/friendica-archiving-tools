import 'package:fediverse_archive_browser/src/models/connection.dart';
import 'package:fediverse_archive_browser/src/services/path_mapper_service_interface.dart';
import 'package:intl/intl.dart';

import 'location_data.dart';
import 'media_attachment.dart';
import 'model_utils.dart';

class TimelineEntry {
  final String id;

  final String parentId;

  final String parentAuthor;

  final String parentAuthorId;

  final int creationTimestamp;

  final int backdatedTimestamp;

  final int modificationTimestamp;

  final String body;

  final String title;

  final bool isReshare;

  final String author;

  final String authorId;

  final String externalLink;

  final List<MediaAttachment> mediaAttachments;

  final LocationData locationData;

  final List<Connection> likes;

  final List<Connection> dislikes;

  final List<Uri> links;

  TimelineEntry({
    this.id = '',
    this.parentId = '',
    this.creationTimestamp = 0,
    this.backdatedTimestamp = 0,
    this.modificationTimestamp = 0,
    this.isReshare = false,
    this.body = '',
    this.title = '',
    this.author = '',
    this.authorId = '',
    this.parentAuthor = '',
    this.parentAuthorId = '',
    this.externalLink = '',
    this.locationData = const LocationData(),
    this.links = const [],
    List<Connection>? likes,
    List<Connection>? dislikes,
    List<MediaAttachment>? mediaAttachments,
  })  : mediaAttachments = mediaAttachments ?? <MediaAttachment>[],
        likes = likes ?? <Connection>[],
        dislikes = dislikes ?? <Connection>[];

  TimelineEntry.randomBuilt()
      : creationTimestamp = DateTime.now().millisecondsSinceEpoch,
        backdatedTimestamp = DateTime.now().millisecondsSinceEpoch,
        modificationTimestamp = DateTime.now().millisecondsSinceEpoch,
        id = randomId(),
        isReshare = false,
        parentId = randomId(),
        externalLink = 'Random external link ${randomId()}',
        body = 'Random post text ${randomId()}',
        title = 'Random title ${randomId()}',
        author = 'Random author ${randomId()}',
        authorId = 'Random authorId ${randomId()}',
        parentAuthor = 'Random parent author ${randomId()}',
        parentAuthorId = 'Random parent author id ${randomId()}',
        locationData = LocationData.randomBuilt(),
        likes = const <Connection>[],
        dislikes = const <Connection>[],
        links = [],
        mediaAttachments = [
          MediaAttachment.randomBuilt(),
          MediaAttachment.randomBuilt()
        ];

  TimelineEntry copy(
      {int? creationTimestamp,
      int? backdatedTimestamp,
      int? modificationTimestamp,
      bool? isReshare,
      String? id,
      String? parentId,
      String? externalLink,
      String? body,
      String? title,
      String? author,
      String? authorId,
      String? parentAuthor,
      String? parentAuthorId,
      LocationData? locationData,
      List<MediaAttachment>? mediaAttachments,
      List<Connection>? likes,
      List<Connection>? dislikes,
      List<Uri>? links}) {
    return TimelineEntry(
      creationTimestamp: creationTimestamp ?? this.creationTimestamp,
      backdatedTimestamp: backdatedTimestamp ?? this.backdatedTimestamp,
      modificationTimestamp:
          modificationTimestamp ?? this.modificationTimestamp,
      id: id ?? this.id,
      isReshare: isReshare ?? this.isReshare,
      parentId: parentId ?? this.parentId,
      externalLink: externalLink ?? this.externalLink,
      body: body ?? this.body,
      title: title ?? this.title,
      author: author ?? this.author,
      authorId: authorId ?? this.authorId,
      parentAuthor: parentAuthor ?? this.parentAuthor,
      parentAuthorId: parentAuthorId ?? this.parentAuthorId,
      locationData: locationData ?? this.locationData,
      mediaAttachments: mediaAttachments ?? this.mediaAttachments,
      likes: likes ?? this.likes,
      dislikes: dislikes ?? this.dislikes,
      links: links ?? this.links,
    );
  }

  @override
  String toString() {
    return 'TimelineEntry{id: $id, isReshare: $isReshare, parentId: $parentId, creationTimestamp: $creationTimestamp, modificationTimestamp: $modificationTimestamp, backdatedTimeStamp: $backdatedTimestamp, post: $body, title: $title, author: $author, parentAuthor: $parentAuthor mediaAttachments: $mediaAttachments, externalLink:$externalLink}';
  }

  String toHumanString(PathMappingService mapper, DateFormat formatter) {
    final creationDateString = formatter.format(
        DateTime.fromMillisecondsSinceEpoch(creationTimestamp * 1000)
            .toLocal());
    return [
      'Title: $title',
      'Creation At: $creationDateString',
      'Text:',
      'Author: $author',
      'Reshare: $isReshare',
      if (externalLink.isNotEmpty) 'External Link: $externalLink',
      body,
      '',
      if (parentId.isNotEmpty)
        "Comment on post/comment by ${parentAuthor.isNotEmpty ? parentAuthor : 'unknown author'}",
      '',
      if (mediaAttachments.isNotEmpty) 'Photos and Videos:',
      ...mediaAttachments.map((e) => e.toHumanString(mapper)),
      if (locationData.hasPosition) locationData.toHumanString(),
      if (links.isNotEmpty) ...['Links:', ...links.map((e) => e.toString())]
    ].join('\n');
  }

  bool hasImages() => mediaAttachments
      .where((element) => element.explicitType == AttachmentMediaType.image)
      .isNotEmpty;

  bool hasVideos() => mediaAttachments
      .where((element) => element.explicitType == AttachmentMediaType.video)
      .isNotEmpty;
}
