import 'package:logging/logging.dart';
import 'package:uuid/uuid.dart';

void logAdditionalKeys<K>(Iterable<K> expectedSet, Iterable<K> actualSet,
    Logger logger, Level level, String label) {
  if (!logger.isLoggable(level)) {
    return;
  }

  final extraKeys =
      actualSet.where((element) => !expectedSet.contains(element));

  for (var k in extraKeys) {
    logger.log(level, '$label: $k');
  }
}

String randomId() => const Uuid().v4();

bool timestampInRange(int timestampinMS, DateTime start, DateTime stop) {
  final startMS = start.millisecondsSinceEpoch;
  final stopMS = stop.millisecondsSinceEpoch;

  return timestampinMS >= startMS && timestampinMS <= stopMS;
}

bool dateTimeInRange(DateTime timestamp, DateTime start, DateTime stop) {
  final timestampMS = timestamp.millisecondsSinceEpoch;
  final startMS = start.millisecondsSinceEpoch;
  final stopMS = stop.millisecondsSinceEpoch;

  return timestampMS >= startMS && timestampMS <= stopMS;
}

RegExp wholeWordRegEx(String word) => RegExp('\\b$word\\b');
