class Connection {
  final ConnectionStatus status;

  final String name;

  final String id;

  final Uri profileUrl;

  final String network;

  Connection(
      {this.status = ConnectionStatus.none,
      this.name = '',
      this.id = '',
      profileUrl,
      this.network = ''})
      : profileUrl = profileUrl ?? Uri();

  @override
  String toString() {
    return 'Connection{status: $status, name: $name, id: $id, profileUrl: $profileUrl, network: $network}';
  }
}

enum ConnectionStatus {
  youFollowThem,
  theyFollowYou,
  mutual,
  none,
}

extension FriendStatusWriter on ConnectionStatus {
  String name() {
    switch (this) {
      case ConnectionStatus.youFollowThem:
        return "You Follow Them";
      case ConnectionStatus.theyFollowYou:
        return "They Follow You";
      case ConnectionStatus.mutual:
        return "Follow each other";
      case ConnectionStatus.none:
        return "Not connected";
    }
  }
}
