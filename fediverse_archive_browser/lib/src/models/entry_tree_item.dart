import 'package:fediverse_archive_browser/src/models/timeline_entry.dart';

class EntryTreeItem {
  final TimelineEntry entry;
  final bool isOrphaned;

  final _children = <String, EntryTreeItem>{};

  EntryTreeItem(this.entry, this.isOrphaned);

  String get id => entry.id;

  void addChild(EntryTreeItem child) {
    _children[child.id] = child;
  }

  List<EntryTreeItem> get children => List.unmodifiable(_children.values);
}
