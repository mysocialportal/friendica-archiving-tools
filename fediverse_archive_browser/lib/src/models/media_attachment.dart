import 'dart:io';

import 'package:fediverse_archive_browser/src/services/path_mapper_service_interface.dart';

import 'model_utils.dart';

enum AttachmentMediaType { unknown, image, video }

class MediaAttachment {
  static final _graphicsExtensions = ['jpg', 'png', 'gif', 'tif'];
  static final _movieExtensions = ['avi', 'mp4', 'mpg', 'wmv'];

  final Uri uri;

  final int creationTimestamp;

  final Map<String, String> metadata;

  final AttachmentMediaType explicitType;

  final Uri thumbnailUri;

  final String title;

  final String description;

  MediaAttachment(
      {required this.uri,
      required this.creationTimestamp,
      required this.metadata,
      required this.thumbnailUri,
      required this.title,
      required this.explicitType,
      required this.description});

  MediaAttachment.randomBuilt()
      : uri = Uri.parse('http://localhost/${randomId()}'),
        creationTimestamp = DateTime.now().millisecondsSinceEpoch,
        title = 'Random title ${randomId()}',
        thumbnailUri = Uri.parse('${randomId()}.jpg'),
        description = 'Random description ${randomId()}',
        explicitType = AttachmentMediaType.image,
        metadata = {'value1': randomId(), 'value2': randomId()};

  MediaAttachment.fromUriOnly(this.uri)
      : creationTimestamp = 0,
        thumbnailUri = Uri.file(''),
        title = '',
        explicitType = mediaTypeFromString(uri.path),
        description = '',
        metadata = {};

  MediaAttachment.fromUriAndTime(this.uri, this.creationTimestamp)
      : thumbnailUri = Uri.file(''),
        title = '',
        explicitType = mediaTypeFromString(uri.path),
        description = '',
        metadata = {};

  MediaAttachment.blank()
      : uri = Uri(),
        creationTimestamp = 0,
        thumbnailUri = Uri.file(''),
        explicitType = AttachmentMediaType.unknown,
        title = '',
        description = '',
        metadata = {};

  @override
  String toString() {
    return 'FriendicaMediaAttachment{uri: $uri, creationTimestamp: $creationTimestamp, type: $explicitType, metadata: $metadata, title: $title, description: $description}';
  }

  String toHumanString(PathMappingService mapper) {
    if (uri.scheme.startsWith('http')) {
      return uri.toString();
    }

    return mapper.toFullPath(uri.toString());
  }

  Map<String, dynamic> toJson() => {
        'uri': uri.toString(),
        'creationTimestamp': creationTimestamp,
        'metadata': metadata,
        'type': explicitType,
        'thumbnailUri': thumbnailUri.toString(),
        'title': title,
        'description': description,
      };

  static AttachmentMediaType mediaTypeFromString(String path) {
    final separator = Platform.isWindows ? '\\' : '/';
    final lastSlash = path.lastIndexOf(separator) + 1;
    final filename = path.substring(lastSlash);
    final lastPeriod = filename.lastIndexOf('.') + 1;
    if (lastPeriod == 0) {
      return AttachmentMediaType.unknown;
    }

    final extension = filename.substring(lastPeriod).toLowerCase();

    if (_graphicsExtensions.contains(extension)) {
      return AttachmentMediaType.image;
    }

    if (_movieExtensions.contains(extension)) {
      return AttachmentMediaType.video;
    }

    return AttachmentMediaType.unknown;
  }
}
