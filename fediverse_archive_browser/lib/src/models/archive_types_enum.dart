enum ArchiveType {
  unknown,
  diaspora,
  friendica,
  mastodon,
}
