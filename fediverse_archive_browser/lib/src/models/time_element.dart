import 'package:fediverse_archive_browser/src/models/timeline_entry.dart';

class TimeElement {
  final DateTime timestamp;
  final TimelineEntry entry;

  TimeElement({int timeInMS = 0, required this.entry})
      : timestamp = DateTime.fromMillisecondsSinceEpoch(timeInMS);

  bool get hasImages => entry.hasImages();

  bool get hasVideos => entry.hasVideos();

  String get text => entry.body;

  String get title => entry.title;

  bool hasText(String phrase) =>
      text.contains(phrase) || title.contains(phrase);
}
