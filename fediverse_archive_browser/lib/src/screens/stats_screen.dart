import 'package:flutter/material.dart';
import 'package:fediverse_archive_browser/src/components/filter_control_component.dart';
import 'package:fediverse_archive_browser/src/components/heatmap_widget.dart';
import 'package:fediverse_archive_browser/src/components/timechart_widget.dart';
import 'package:fediverse_archive_browser/src/components/top_interactactors_widget.dart';
import 'package:fediverse_archive_browser/src/components/word_frequency_widget.dart';
import 'package:fediverse_archive_browser/src/models/model_utils.dart';
import 'package:fediverse_archive_browser/src/models/time_element.dart';
import 'package:fediverse_archive_browser/src/screens/standin_status_screen.dart';
import 'package:fediverse_archive_browser/src/services/archive_service_provider.dart';
import 'package:fediverse_archive_browser/src/utils/snackbar_status_builder.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';

class StatsScreen extends StatefulWidget {
  const StatsScreen({Key? key}) : super(key: key);

  @override
  State<StatsScreen> createState() => _StatsScreenState();
}

class _StatsScreenState extends State<StatsScreen> {
  static final _logger = Logger("$_StatsScreenState");
  ArchiveServiceProvider? archiveDataService;
  final allItems = <TimeElement>[];
  StatType statType = StatType.selectType;
  bool hasText = true;

  @override
  void initState() {
    super.initState();
  }

  void _updateSelection(BuildContext context, StatType newType) async {
    statType = newType;
    await _updateItems(context);
    setState(() {});
  }

  Future<void> _updateItems(BuildContext context) async {
    if (archiveDataService == null) {
      _logger.severe(
          "Can't update stats because archive data service is not set yet");
    }
    allItems.clear();
    Iterable<TimeElement> newItems = [];
    switch (statType) {
      case StatType.post:
        newItems = (await archiveDataService!.getPosts()).fold(
            onSuccess: (posts) => posts.map((e) => TimeElement(
                timeInMS: e.entry.creationTimestamp * 1000, entry: e.entry)),
            onError: (error) {
              _logger.severe('Error getting posts: $error');
              return [];
            });
        break;
      case StatType.comment:
        newItems = (await archiveDataService!.getAllComments()).fold(
            onSuccess: (comments) => comments.map((e) => TimeElement(
                timeInMS: e.entry.creationTimestamp * 1000, entry: e.entry)),
            onError: (error) {
              _logger.severe('Error getting comments: $error');
              return [];
            });
        break;
      case StatType.selectType:
        break;
      default:
        _logger.severe('Unknown stat type');
        Future.delayed(
            Duration.zero,
            () => SnackBarStatusBuilder.buildSnackbar(
                context, 'Unknown stat type'));
    }

    allItems.addAll(newItems);
  }

  @override
  Widget build(BuildContext context) {
    archiveDataService = Provider.of<ArchiveServiceProvider>(context);

    return FilterControl<TimeElement, dynamic>(
        allItems: allItems,
        imagesOnlyFilterFunction: (item) => item.hasImages,
        videosOnlyFilterFunction: (item) => item.hasVideos,
        textSearchFilterFunction: (item, text) => item.hasText(text),
        itemToDateTimeFunction: (item) => item.timestamp,
        dateRangeFilterFunction: (item, start, stop) =>
            dateTimeInRange(item.timestamp, start, stop),
        builder: (context, items) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              constraints: const BoxConstraints(maxWidth: 800),
              child: Column(
                children: [
                  Row(
                    children: [
                      const Text('Statistic Type: '),
                      DropdownButton(
                          hint: const Text('Select type'),
                          value:
                              statType == StatType.selectType ? null : statType,
                          onChanged: (StatType? type) =>
                              _updateSelection(context, type!),
                          items: StatType.values
                              .map((value) => DropdownMenuItem(
                                  enabled: value != StatType.selectType,
                                  value: value,
                                  child: Text(value.toLabel())))
                              .where((element) => element.enabled)
                              .toList()),
                    ],
                  ),
                  statType == StatType.selectType
                      ? const Expanded(
                          child: StandInStatusScreen(
                              title: 'Select data type to show graphs'),
                        )
                      : _buildChartPanel(context, items)
                ],
              ),
            ),
          );
        });
  }

  Widget _buildChartPanel(BuildContext context, List<TimeElement> items) {
    if (items.isEmpty) {
      return Expanded(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            StandInStatusScreen(
              title: 'No items for statistics',
              subTitle: 'Adjust the filter or select a new archive',
            )
          ],
        ),
      );
    }

    return Expanded(
        child: SingleChildScrollView(
      primary: false,
      child: Column(children: [
        ..._buildGraphScreens(context, items),
        const Divider(),
        TopInteractorsWidget(items, archiveDataService!.connectionsManager),
        const Divider(),
        WordFrequencyWidget(items),
      ]),
    ));
  }

  List<Widget> _buildGraphScreens(
      BuildContext context, List<TimeElement> items) {
    return [
      TimeChartWidget(timeElements: items),
      const Divider(),
      HeatMapWidget(timeElements: items),
    ];
  }
}

enum StatType {
  post,
  comment,
  selectType,
}

extension StatTypeString on StatType {
  String toLabel() {
    switch (this) {
      case StatType.post:
        return "Posts";
      case StatType.comment:
        return "Comments";
      case StatType.selectType:
        return "Select Type";
    }
  }

  StatType fromLabel(String text) {
    if (text == 'Posts') {
      return StatType.post;
    }

    if (text == 'Comments') {
      return StatType.comment;
    }

    if (text == 'Select Type') {
      return StatType.selectType;
    }

    throw ArgumentError(['Unknown enum type: $text', 'text']);
  }
}
