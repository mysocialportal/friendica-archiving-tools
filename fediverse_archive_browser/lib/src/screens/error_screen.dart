import 'package:flutter/material.dart';
import 'package:fediverse_archive_browser/src/settings/settings_controller.dart';
import 'package:fediverse_archive_browser/src/utils/exec_error.dart';
import 'package:provider/provider.dart';

class ErrorScreen extends StatelessWidget {
  final ExecError error;
  final String title;

  const ErrorScreen(
      {Key? key, this.title = 'Error executing', required this.error})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logPath = Provider.of<SettingsController>(context).logPath;
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          title,
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.headline6,
          softWrap: true,
        ),
        const SizedBox(height: 5),
        SelectableText('See logfile for more details: $logPath'),
        const SizedBox(height: 5),
        if (error.exception != null)
          SelectableText('Error with exception: ${error.exception}'),
        const SizedBox(height: 5),
        if (error.errorMessage.isNotEmpty) SelectableText(error.errorMessage),
      ],
    ));
  }
}
