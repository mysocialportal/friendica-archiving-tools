import 'package:fediverse_archive_browser/src/components/filter_control_component.dart';
import 'package:fediverse_archive_browser/src/components/tree_entry_card.dart';
import 'package:fediverse_archive_browser/src/models/entry_tree_item.dart';
import 'package:fediverse_archive_browser/src/models/model_utils.dart';
import 'package:fediverse_archive_browser/src/screens/error_screen.dart';
import 'package:fediverse_archive_browser/src/settings/settings_controller.dart';
import 'package:fediverse_archive_browser/src/utils/exec_error.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';
import 'package:result_monad/result_monad.dart';

import 'loading_status_screen.dart';
import 'standin_status_screen.dart';

class EntriesScreen extends StatelessWidget {
  static final _logger = Logger('$EntriesScreen');
  final FutureResult<List<EntryTreeItem>, ExecError> Function() populator;

  const EntriesScreen({Key? key, required this.populator}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _logger.info('Build FriendicaEntriesScreen');
    Provider.of<SettingsController>(context);

    return FutureBuilder<Result<List<EntryTreeItem>, ExecError>>(
        future: populator(),
        builder: (context, snapshot) {
          _logger.info('FriendicaEntriesScreen Future builder called');

          if (!snapshot.hasData ||
              snapshot.connectionState != ConnectionState.done) {
            return const LoadingStatusScreen(title: 'Loading entries');
          }

          final postsResult = snapshot.requireData;

          if (postsResult.isFailure) {
            return ErrorScreen(
                title: 'Error getting entries', error: postsResult.error);
          }

          final allPosts = postsResult.value;
          final posts = allPosts;

          if (posts.isEmpty) {
            return const StandInStatusScreen(title: 'No entries were found');
          }

          _logger.fine('Build Entries ListView');
          return _FriendicaEntriesScreenWidget(posts: posts);
        });
  }
}

class _FriendicaEntriesScreenWidget extends StatelessWidget {
  static final _logger = Logger('$_FriendicaEntriesScreenWidget');

  final List<EntryTreeItem> posts;

  const _FriendicaEntriesScreenWidget({Key? key, required this.posts})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    _logger.fine('Redrawing');
    return FilterControl<EntryTreeItem, dynamic>(
        allItems: posts,
        commentsOnlyFilterFunction: (post) => post.children.isNotEmpty,
        imagesOnlyFilterFunction: (post) => post.entry.hasImages(),
        videosOnlyFilterFunction: (post) => post.entry.hasVideos(),
        textSearchFilterFunction: (post, text) =>
            post.entry.title.contains(text) || post.entry.body.contains(text),
        itemToDateTimeFunction: (post) => DateTime.fromMillisecondsSinceEpoch(
            post.entry.creationTimestamp * 1000),
        dateRangeFilterFunction: (post, start, stop) =>
            timestampInRange(post.entry.creationTimestamp * 1000, start, stop),
        builder: (context, items) {
          if (items.isEmpty) {
            return const StandInStatusScreen(
                title: 'No posts meet filter criteria');
          }

          return ScrollConfiguration(
            behavior:
                ScrollConfiguration.of(context).copyWith(scrollbars: false),
            child: ListView.separated(
                primary: false,
                physics: const RangeMaintainingScrollPhysics(),
                restorationId: 'friendicaEntriesListView',
                itemCount: items.length,
                itemBuilder: (context, index) {
                  _logger.finer('Rendering Friendica List Item');
                  return TreeEntryCard(
                    treeEntry: items[index],
                    isTopLevel: true,
                  );
                },
                separatorBuilder: (context, index) {
                  return const Divider(
                    color: Colors.black,
                    thickness: 0.2,
                  );
                }),
          );
        });
  }
}
