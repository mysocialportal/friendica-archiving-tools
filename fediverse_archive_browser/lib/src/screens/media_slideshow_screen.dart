import 'dart:io';

import 'package:fediverse_archive_browser/src/services/archive_service_provider.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fediverse_archive_browser/src/components/media_wrapper_component.dart';
import 'package:fediverse_archive_browser/src/models/media_attachment.dart';
import 'package:fediverse_archive_browser/src/settings/settings_controller.dart';
import 'package:fediverse_archive_browser/src/themes.dart';
import 'package:fediverse_archive_browser/src/utils/snackbar_status_builder.dart';
import 'package:provider/provider.dart';

class MediaSlideShowScreen extends StatefulWidget {
  static const _spacing = 5.0;

  final List<MediaAttachment> mediaAttachments;
  final int initialIndex;

  const MediaSlideShowScreen(
      {Key? key, required this.mediaAttachments, required this.initialIndex})
      : super(key: key);

  @override
  State<MediaSlideShowScreen> createState() => _MediaSlideShowScreenState();
}

class _MediaSlideShowScreenState extends State<MediaSlideShowScreen> {
  static const fastestChangeMS = 250;
  MediaAttachment media = MediaAttachment.blank();
  int index = 0;
  int lastKeyInducedIndexChange = 0;

  @override
  void initState() {
    index = widget.initialIndex;
    media = widget.mediaAttachments[index];
    super.initState();
  }

  void updateIndex(int newIndex) {
    setState(() {
      index = newIndex;
      media = widget.mediaAttachments[index];
    });
  }

  void previousImage() {
    if (index == 0) {
      return;
    }

    updateIndex(--index);
  }

  void nextImage() {
    if (index == widget.mediaAttachments.length - 1) {
      return;
    }

    updateIndex(++index);
  }

  @override
  Widget build(BuildContext context) {
    final formatter =
        Provider.of<SettingsController>(context).dateTimeFormatter;

    const toolBarHeight = 50.0;
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height - toolBarHeight;

    return Theme(
      data: FediverseArchiveBrowserTheme.darkroom,
      child: KeyboardListener(
        focusNode: FocusNode(),
        autofocus: true,
        onKeyEvent: (event) {
          final key = event.logicalKey;
          final now = DateTime.now().millisecondsSinceEpoch;
          if (key == LogicalKeyboardKey.arrowLeft) {
            if (now - lastKeyInducedIndexChange >= fastestChangeMS) {
              previousImage();
              lastKeyInducedIndexChange = now;
            }
          } else if (key == LogicalKeyboardKey.arrowRight) {
            if (now - lastKeyInducedIndexChange >= fastestChangeMS) {
              nextImage();
              lastKeyInducedIndexChange = now;
            }
          } else if (key == LogicalKeyboardKey.escape) {
            Navigator.of(context).pop();
          }
        },
        child: Scaffold(
          appBar: AppBar(
            toolbarHeight: toolBarHeight,
            title: Text(media.title),
            elevation: 0.0,
          ),
          body: Stack(
            children: [
              Positioned(
                width: width,
                height: height,
                child: Center(
                    child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: MediaWrapperComponent(
                          mediaAttachment: media,
                        ),
                      ),
                      const SizedBox(height: MediaSlideShowScreen._spacing),
                      SelectableText(media.description),
                      const SizedBox(height: MediaSlideShowScreen._spacing),
                      SelectableText(
                        formatter.format(DateTime.fromMillisecondsSinceEpoch(
                            media.creationTimestamp * 1000)),
                        style: const TextStyle(
                          fontStyle: FontStyle.italic,
                        ),
                      ),
                    ],
                  ),
                )),
              ),
              Container(
                  width: width,
                  alignment: Alignment.centerLeft,
                  child: TextButton(
                      onPressed: index == 0 ? null : previousImage,
                      child: const Icon(Icons.arrow_back))),
              Container(
                  width: width,
                  alignment: Alignment.centerRight,
                  child: TextButton(
                      onPressed: index == widget.mediaAttachments.length - 1
                          ? null
                          : nextImage,
                      child: const Icon(Icons.arrow_forward))),
            ],
          ),
          floatingActionButton: FloatingActionButton(
              onPressed: () => _saveFile(context),
              tooltip: 'Save file to disk',
              child: const Icon(Icons.save)),
        ),
      ),
    );
  }

  Future<void> _saveFile(BuildContext context) async {
    final archiveService =
        Provider.of<ArchiveServiceProvider>(context, listen: false);

    final filename = media.uri.pathSegments.last;
    final localPath = archiveService.getImageByUrl(media.uri.toString());
    if (localPath.isFailure) {
      SnackBarStatusBuilder.buildSnackbar(context, 'Unable to find original source file for: ${media.uri}');
      return;
    }
    final initialPath = archiveService.pathMappingService.toFullPath(localPath.value.localFilename);
    final newPath = await FilePicker.platform.saveFile(
      dialogTitle: 'Export Image',
      fileName: filename,
    );

    if (newPath == null) {
      return;
    }

    final initialFile = File(initialPath);
    final copiedFile = await initialFile.copy(newPath);
    final copiedFileExists = await copiedFile.exists();

    final message = copiedFileExists
        ? 'File exported to: $newPath'
        : 'Error exporting file to: $newPath';

    SnackBarStatusBuilder.buildSnackbar(context, message);
  }
}
