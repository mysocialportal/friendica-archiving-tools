import 'package:flutter/material.dart';
import 'package:fediverse_archive_browser/src/models/time_element.dart';
import 'package:fediverse_archive_browser/src/utils/word_map_generator.dart';
import 'package:logging/logging.dart';

class WordFrequencyWidget extends StatefulWidget {
  final List<TimeElement> elements;

  const WordFrequencyWidget(this.elements, {Key? key}) : super(key: key);

  @override
  State<WordFrequencyWidget> createState() => _WordFrequencyWidgetState();
}

class _WordFrequencyWidgetState extends State<WordFrequencyWidget> {
  static final _logger = Logger('$WordFrequencyWidget');
  int _currentThreshold = 10;
  final _thresholds = [10, 20, 50, 100];
  final topElements = <WordMapItem>[];
  final generator = WordMapGenerator.withCommonWordsFilter(minimumWordSize: 3);

  @override
  void initState() {
    super.initState();
  }

  // TODO: Put in Isolate if jank goes for too long in practice
  void _generateWordMap() {
    _logger.finer('Filling list');
    generator.clear();
    for (final item in widget.elements) {
      generator.processEntry(item.text);
    }
    _logger.finer('List filled');
    _calcTopList(false);
  }

  Future<void> _calcTopList(bool updateState) async {
    final newTopElements = generator.getTopList(_currentThreshold);
    topElements.clear();
    topElements.addAll(newTopElements);
    if (updateState) {
      setState(() {});
    }

    _logger.finer('List filled with ${topElements.length} elements');
  }

  @override
  Widget build(BuildContext context) {
    _logger.fine('Rebuilding WordFrequencyWidget');
    _generateWordMap();

    _logger.finer('Top elements count: ${topElements.length}');

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text(
                'Top',
                textAlign: TextAlign.left,
                style: Theme.of(context).textTheme.headline6,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 5.0, right: 5.0),
                child: DropdownButton<int>(
                    value: _currentThreshold,
                    items: _thresholds
                        .map((t) =>
                            DropdownMenuItem(value: t, child: Text('$t')))
                        .toList(),
                    onChanged: (newValue) async {
                      _currentThreshold = newValue ?? _thresholds.first;
                      _calcTopList(true);
                    }),
              ),
              Text(
                'Words',
                textAlign: TextAlign.right,
                style: Theme.of(context).textTheme.headline6,
              ),
            ],
          ),
          const SizedBox(height: 10.0),
          _buildDataTable(context),
        ],
      ),
    );
  }

  Widget _buildDataTable(BuildContext context) {
    return DataTable(
      sortAscending: false,
      columns: const [
        DataColumn(label: Text('Word')),
        DataColumn(label: Text('Count'), numeric: true),
      ],
      rows: List.generate(
          topElements.length,
          (index) => DataRow(
                  color: index.isEven
                      ? MaterialStateProperty.resolveWith(
                          (states) => Theme.of(context).dividerColor)
                      : null,
                  cells: [
                    DataCell(Text(topElements[index].word)),
                    DataCell(Text('${topElements[index].count}')),
                  ])),
    );
  }
}
