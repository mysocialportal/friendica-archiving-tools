import 'dart:math';
import 'dart:ui';

import 'package:latlng/latlng.dart';
import 'package:map/map.dart';

class MapBounds {
  static final globe = MapBounds(
      upperLeft: LatLng(85.0, -180.0),
      lowerRight: LatLng(-85, 180.0),
      idealCenterPoint: LatLng(0.0, 0.0));
  final LatLng upperLeft;
  final LatLng lowerRight;
  final LatLng idealCenterPoint;

  MapBounds(
      {required this.upperLeft,
      required this.lowerRight,
      required this.idealCenterPoint});

  static MapBounds computed(MapTransformer transformer) {
    final mapSize = transformer.constraints.biggest;
    final upperLeft = transformer.toLatLng(Offset.zero);
    final lowerRight =
        transformer.toLatLng(Offset(mapSize.width, mapSize.height));
    final idealLeftLongitude = max(-180.0, upperLeft.longitude);
    final idealRightLongitude = min(180.0, lowerRight.longitude);
    final idealUpperLatitude = min(85.0, upperLeft.latitude);
    final idealLowerLatitude = max(-85.0, lowerRight.latitude);
    final idealCenterLatLon = LatLng(
        (idealUpperLatitude + idealLowerLatitude) / 2.0,
        (idealRightLongitude + idealLeftLongitude) / 2.0);

    return MapBounds(
        upperLeft: upperLeft,
        lowerRight: lowerRight,
        idealCenterPoint: idealCenterLatLon);
  }

  bool pointInBounds(double latitude, double longitude) {
    if (latitude > upperLeft.latitude || latitude < lowerRight.latitude) {
      return false;
    }

    if (longitude < upperLeft.longitude || longitude > lowerRight.longitude) {
      return false;
    }

    return true;
  }

  bool isOverflowedUpperLeft() {
    if (upperLeft.longitude < -180.0) {
      return true;
    }

    if (upperLeft.latitude > 85.0) {
      return true;
    }

    return false;
  }

  bool isOverflowedLowerRight() {
    if (lowerRight.latitude < -85.0) {
      return true;
    }

    if (lowerRight.longitude > 180.0) {
      return true;
    }

    return false;
  }

  bool isOverflowed() => isOverflowedUpperLeft() || isOverflowedLowerRight();

  @override
  String toString() {
    return 'UpperLeft: (${upperLeft.latitude},${upperLeft.longitude}); LowerRight: (${lowerRight.latitude},${lowerRight.longitude}); overflowed: ${isOverflowed()}';
  }
}
