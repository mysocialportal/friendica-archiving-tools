import 'dart:ui';

import 'package:fediverse_archive_browser/src/models/timeline_entry.dart';
import 'package:latlng/latlng.dart';
import 'package:map/map.dart';

import 'marker_data.dart';

extension GeoSpatialPostExtensions on TimelineEntry {
  MarkerData toMarkerData(MapTransformer transformer, Color color) {
    final latLon = LatLng(locationData.latitude, locationData.longitude);
    final offset = transformer.toOffset(latLon);
    return MarkerData(this, offset, color);
  }
}
