import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:fediverse_archive_browser/src/models/entry_tree_item.dart';
import 'package:fediverse_archive_browser/src/models/location_data.dart';
import 'package:fediverse_archive_browser/src/services/archive_service_provider.dart';
import 'package:fediverse_archive_browser/src/settings/settings_controller.dart';
import 'package:fediverse_archive_browser/src/utils/clipboard_helper.dart';
import 'package:fediverse_archive_browser/src/utils/snackbar_status_builder.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import 'link_elements_component.dart';
import 'media_timeline_component.dart';

class TreeEntryCard extends StatelessWidget {
  static final _logger = Logger("$TreeEntryCard");
  final EntryTreeItem treeEntry;
  final bool isTopLevel;

  const TreeEntryCard(
      {Key? key, required this.treeEntry, this.isTopLevel = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    const double spacingHeight = 5.0;
    final formatter =
        Provider.of<SettingsController>(context).dateTimeFormatter;
    final archiveService = Provider.of<ArchiveServiceProvider>(context);

    final entry = treeEntry.entry;

    final title = entry.title.isNotEmpty
        ? entry.title
        : entry.parentId.isEmpty
            ? (entry.isReshare ? 'Reshare' : 'Post')
            : 'Comment on post by ${entry.author}';
    final dateStamp = ' At ' +
        formatter.format(
            DateTime.fromMillisecondsSinceEpoch(entry.creationTimestamp * 1000)
                .toLocal());

    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        color: !isTopLevel ? Theme.of(context).dividerColor : null,
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Wrap(
                  direction: Axis.horizontal,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    Text(
                      title,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(dateStamp,
                        style: const TextStyle(
                          fontStyle: FontStyle.italic,
                        )),
                    Tooltip(
                      message: 'Copy text version of post to clipboard',
                      child: IconButton(
                          onPressed: () async => await copyToClipboard(
                              context: context,
                              text: entry.toHumanString(
                                  archiveService.pathMappingService, formatter),
                              snackbarMessage: 'Copied Post to clipboard'),
                          icon: const Icon(Icons.copy)),
                    ),
                    Tooltip(
                      message:
                          'Open link to original item (${entry.externalLink})',
                      child: IconButton(
                          onPressed: () async {
                            await canLaunch(entry.externalLink)
                                ? await launch(entry.externalLink)
                                : _logger.info(
                                    'Failed to launch ${entry.externalLink}');
                          },
                          icon: const Icon(Icons.link)),
                    ),
                  ]),
              if (entry.body.isNotEmpty) ...[
                const SizedBox(height: spacingHeight),
                HtmlWidget(
                  entry.body,
                  onTapUrl: (url) async {
                    bool canLaunchResult = await canLaunch(url);
                    if (!canLaunchResult) {
                      return false;
                    }

                    bool launched = await launch(url);
                    if (!launched) {
                      final message = 'Failed to launch: $url';
                      _logger.info(message);
                      SnackBarStatusBuilder.buildSnackbar(context, message);
                    }
                    return launched;
                  },
                )
              ],
              const SizedBox(height: spacingHeight * 2),
              Row(
                children: [
                  Tooltip(
                      message: entry.likes.map((e) => e.name).join(', '),
                      child: const Icon(Icons.thumb_up_alt_outlined)),
                  Text('${entry.likes.length}'),
                  SizedBox(
                    width: 3,
                  ),
                  Tooltip(
                      message: entry.dislikes.map((e) => e.name).join(', '),
                      child: const Icon(Icons.thumb_down_alt_outlined)),
                  Text('${entry.dislikes.length}'),
                ],
              ),
              if (entry.locationData.hasData())
                entry.locationData.toWidget(spacingHeight),
              if (treeEntry.entry.links.isNotEmpty) ...[
                const SizedBox(height: spacingHeight),
                LinkElementsComponent(links: treeEntry.entry.links)
              ],
              if (entry.mediaAttachments.isNotEmpty) ...[
                const SizedBox(height: spacingHeight),
                MediaTimelineComponent(mediaAttachments: entry.mediaAttachments)
              ],
              if (treeEntry.children.isNotEmpty)
                Column(
                  children: treeEntry.children
                      .map((e) => TreeEntryCard(
                            treeEntry: e,
                            isTopLevel: false,
                          ))
                      .toList(),
                )
            ],
          ),
        ),
      ),
    );
  }
}
