import 'dart:math';

import 'package:flutter/material.dart';
import 'package:fediverse_archive_browser/src/models/media_attachment.dart';
import 'package:fediverse_archive_browser/src/screens/media_slideshow_screen.dart';
import 'package:fediverse_archive_browser/src/services/archive_service_provider.dart';
import 'package:fediverse_archive_browser/src/settings/settings_controller.dart';
import 'package:provider/provider.dart';

import 'media_wrapper_component.dart';

class MediaTimelineComponent extends StatelessWidget {
  static const double _maxHeightWidth = 400.0;

  final List<MediaAttachment> mediaAttachments;

  const MediaTimelineComponent({Key? key, required this.mediaAttachments})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (mediaAttachments.isEmpty) {
      return const SizedBox(width: 0, height: 0);
    }

    final bool isSingle = mediaAttachments.length == 1;
    final double singleWidth = MediaQuery.of(context).size.width / 2.0;
    final double threeAcrossWidth = MediaQuery.of(context).size.width / 3.0;
    final double preferredMultiWidth = min(threeAcrossWidth, _maxHeightWidth);
    final archiveService = Provider.of<ArchiveServiceProvider>(context);
    final settingsController = Provider.of<SettingsController>(context);

    return Container(
      constraints: const BoxConstraints(
        maxHeight: _maxHeightWidth,
      ),
      child: ListView.separated(
          // shrinkWrap: true,
          // primary: true,
          scrollDirection: Axis.horizontal,
          itemCount: mediaAttachments.length,
          itemBuilder: (context, index) {
            return InkWell(
              onTap: () async {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return MultiProvider(
                      providers: [
                        ChangeNotifierProvider.value(value: settingsController),
                        ChangeNotifierProvider.value(value: archiveService),
                      ],
                      child: MediaSlideShowScreen(
                          mediaAttachments: mediaAttachments,
                          initialIndex: index));
                }));
              },
              child: MediaWrapperComponent(
                mediaAttachment: mediaAttachments[index],
                preferredWidth: isSingle ? singleWidth : preferredMultiWidth,
              ),
            );
          },
          separatorBuilder: (context, index) {
            return const SizedBox(width: 10);
          }),
    );
  }
}
