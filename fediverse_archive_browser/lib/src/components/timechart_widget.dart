import 'package:flutter/material.dart';
import 'package:fediverse_archive_browser/src/components/barchart_panel.dart';
import 'package:fediverse_archive_browser/src/models/stat_bin.dart';
import 'package:fediverse_archive_browser/src/models/time_element.dart';
import 'package:fediverse_archive_browser/src/screens/standin_status_screen.dart';
import 'package:fediverse_archive_browser/src/utils/time_stat_generator.dart';
import 'package:logging/logging.dart';

class TimeChartWidget extends StatefulWidget {
  final List<TimeElement> timeElements;

  const TimeChartWidget({Key? key, required this.timeElements})
      : super(key: key);

  @override
  State<TimeChartWidget> createState() => _TimeChartWidgetState();
}

class _TimeChartWidgetState extends State<TimeChartWidget> {
  static final _logger = Logger('$_TimeChartWidgetState');
  _TimeType _timeType = _TimeType.year;

  @override
  Widget build(BuildContext context) {
    _logger.fine('Build TimeChartWidget');
    if (widget.timeElements.isEmpty) {
      return const StandInStatusScreen(title: 'No items for statistics');
    }

    final statBins = <StatBin>[];
    final generator = TimeStatGenerator(widget.timeElements);
    late final String Function(int index) xAxisStringFunction;

    switch (_timeType) {
      case _TimeType.day:
        xAxisStringFunction = (index) => _dayStringFromIndex(index);
        statBins.addAll(generator.calculateByDayOfWeekStats());
        break;
      case _TimeType.month:
        xAxisStringFunction = (index) => _monthStringFromIndex(index);
        statBins.addAll(generator.calculateByMonthStats());
        break;
      case _TimeType.year:
        statBins.addAll(generator.calculateStatsByYear());
        xAxisStringFunction = (int index) => index.toString();
        break;
    }

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            '${_timeType.toAdjectiveName()} Statistics',
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headline6,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              const Text('Date Grouping Type:'),
              const SizedBox(width: 5),
              DropdownButton<_TimeType>(
                  value: _timeType,
                  items: _TimeType.values
                      .map((e) =>
                          DropdownMenuItem(value: e, child: Text(e.toName())))
                      .toList(),
                  onChanged: (timeType) => setState(() {
                        _timeType = timeType!;
                      })),
            ],
          ),
          BarChartComponent(stats: statBins, xLabelMaker: xAxisStringFunction)
        ],
      ),
    );
  }

  String _dayStringFromIndex(int index) {
    switch (index) {
      case 1:
        return 'Monday';
      case 2:
        return 'Tuesday';
      case 3:
        return 'Wednesday';
      case 4:
        return 'Thursday';
      case 5:
        return 'Friday';
      case 6:
        return 'Saturday';
      case 7:
        return 'Sunday';
      default:
        _logger.severe(['Invalid date index: $index', 'index']);
        return '$index';
    }
  }

  String _monthStringFromIndex(int index) {
    switch (index) {
      case 1:
        return 'January';
      case 2:
        return 'February';
      case 3:
        return 'March';
      case 4:
        return 'April';
      case 5:
        return 'May';
      case 6:
        return 'June';
      case 7:
        return 'July';
      case 8:
        return 'August';
      case 9:
        return 'September';
      case 10:
        return 'October';
      case 11:
        return 'November';
      case 12:
        return 'December';
      default:
        _logger.severe(['Invalid date index: $index', 'index']);
        return '$index';
    }
  }
}

enum _TimeType { day, month, year }

extension _TimeTypeStringUtils on _TimeType {
  String toAdjectiveName() {
    switch (this) {
      case _TimeType.day:
        return 'Daily';
      case _TimeType.month:
        return 'Monthly';
      case _TimeType.year:
        return 'Yearly';
    }
  }

  String toName() {
    switch (this) {
      case _TimeType.day:
        return 'Day';
      case _TimeType.month:
        return 'Month';
      case _TimeType.year:
        return 'Year';
    }
  }
}
