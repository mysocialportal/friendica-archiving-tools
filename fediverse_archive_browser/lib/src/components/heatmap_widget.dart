import 'package:flutter/material.dart';
import 'package:fediverse_archive_browser/src/models/time_element.dart';
import 'package:fediverse_archive_browser/src/screens/standin_status_screen.dart';
import 'package:fediverse_archive_browser/src/utils/time_stat_generator.dart';

import 'heatmap/heatmap_component.dart';

class HeatMapWidget extends StatefulWidget {
  final List<TimeElement> timeElements;

  const HeatMapWidget({Key? key, required this.timeElements}) : super(key: key);

  @override
  State<HeatMapWidget> createState() => _HeatMapWidgetState();
}

class _HeatMapWidgetState extends State<HeatMapWidget> {
  int year = 2024;
  final years = <int>[];

  @override
  void initState() {
    years.clear();
    final newYears = widget.timeElements.map((e) => e.timestamp.year).toSet();
    if (newYears.isEmpty) {
      years.add(DateTime.now().year);
    }
    years.addAll(newYears);
    years.sort();
    year = years.last;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.timeElements.isEmpty) {
      return const StandInStatusScreen(title: 'No items for heat map');
    }

    final statBins = TimeStatGenerator(widget.timeElements
            .where((element) => element.timestamp.year == year))
        .calculateDailyStats();

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            'Heat Map for $year',
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headline6,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              const Text('Year:'),
              const SizedBox(width: 5),
              DropdownButton<int>(
                  value: year,
                  items: years
                      .map((y) => DropdownMenuItem(value: y, child: Text('$y')))
                      .toList(),
                  onChanged: (newYear) => setState(() {
                        year = newYear!;
                      })),
            ],
          ),
          HeatMapComponent(year: year, stats: statBins),
        ],
      ),
    );
  }
}
