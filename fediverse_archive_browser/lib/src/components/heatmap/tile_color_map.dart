import 'dart:ui';

import 'package:result_monad/result_monad.dart';

class TileColorMap {
  final Map<int, Color> thresholds;
  final Color? zeroValue;
  final thresholdValues = <int>[];

  TileColorMap(this.thresholds, {this.zeroValue}) {
    thresholdValues.addAll(thresholds.keys);
    thresholdValues.sort();
  }

  Result<Color, int> getColor(int value) {
    if (thresholdValues.isEmpty) {
      return Result.error(0);
    }

    if (zeroValue != null && value == 0) {
      return Result.ok(zeroValue!);
    }

    int thresholdIndex = thresholdValues
        .where((element) => element <= value)
        .lastWhere((element) => element <= value,
            orElse: () => thresholdValues.first);
    return Result.ok(thresholds[thresholdIndex]!);
  }
}
