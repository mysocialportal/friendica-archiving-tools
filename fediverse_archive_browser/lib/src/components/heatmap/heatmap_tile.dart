import 'package:flutter/material.dart';
import 'package:fediverse_archive_browser/src/components/heatmap/tile_color_map.dart';

class HeatMapTile extends StatelessWidget {
  static const width = 12.0;
  static const height = 12.0;
  static const margin = 1.0;
  final String dateString;
  final int value;
  final TileColorMap colorMap;

  static double get totalHeight => height + (margin * 2);

  static double get totalWidth => width + (margin * 2);

  const HeatMapTile(this.dateString, this.value, this.colorMap, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final colorResult = colorMap.getColor(value);
    return colorResult.fold(
        onSuccess: (color) => Tooltip(
            message: '$value on $dateString',
            child: Card(
                margin: const EdgeInsets.all(margin),
                color: color,
                child: const SizedBox(width: width, height: width))),
        onError: (error) => Tooltip(
            message: dateString,
            child: const Padding(
              padding: EdgeInsets.all(margin),
              child: SizedBox(width: width, height: height),
            )));
  }

  HeatMapTile.blankTile(this.dateString, {Key? key})
      : value = 0,
        colorMap = TileColorMap({}),
        super(key: key);
}
