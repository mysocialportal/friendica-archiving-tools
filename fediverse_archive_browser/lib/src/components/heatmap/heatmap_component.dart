import 'package:flutter/material.dart';
import 'package:fediverse_archive_browser/src/components/heatmap/heatmap_tile.dart';
import 'package:fediverse_archive_browser/src/components/heatmap/tile_color_map.dart';
import 'package:fediverse_archive_browser/src/models/stat_bin.dart';
import 'package:fediverse_archive_browser/src/settings/settings_controller.dart';
import 'package:fediverse_archive_browser/src/utils/time_stat_generator.dart';
import 'package:provider/provider.dart';

class HeatMapComponent extends StatelessWidget {
  static const gridStart = 40;
  static final colorMapData = {
    1: Colors.green[100]!,
    5: Colors.green[300]!,
    10: Colors.green[500]!,
    20: Colors.green[700]!
  };

  final int year;
  final List<StatBin> stats;

  const HeatMapComponent({Key? key, required this.year, required this.stats})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final formatter = Provider.of<SettingsController>(context).dateFormatter;
    final zeroColor = Theme.of(context).cardColor;
    final colorMap = TileColorMap(colorMapData, zeroValue: zeroColor);

    final statsByDay = <DateTime, int>{};
    for (final stat in stats) {
      statsByDay[stat.binEpoch] = stat.count;
    }

    final firstDayOfCalendar = _firstHeatMapDay();
    final weeks = List.generate(
            53,
            (index) =>
                firstDayOfCalendar.add(Duration(days: 7 * index)).toDayOnly())
        .where((date) => date.year <= year)
        .toList();
    final weekColumns = weeks
        .map((week) => Column(
                children: List.generate(7, (day) {
              final currentDate = week.add(Duration(days: day));
              final value = statsByDay[currentDate] ?? 0;
              if (currentDate.year != year) {
                return HeatMapTile.blankTile(formatter.format(currentDate));
              }
              return HeatMapTile(
                  formatter.format(currentDate), value, colorMap);
            })))
        .toList();

    final dayofWeekColumn = _buildDayOfWeekLabels(context);

    final monthsOfYearRow = SizedBox(
        height: 20,
        width: 800,
        child: Stack(
          children: _buildMonthLabels(weeks),
        ));

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        monthsOfYearRow,
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [dayofWeekColumn, ...weekColumns],
        ),
        _buildLegendWidget(context, colorMap),
      ],
    );
  }

  Widget _buildLegendWidget(BuildContext context, TileColorMap colorMap) {
    final legend = [
      Row(
        children: const [
          Text(
            'Legend',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ],
      ),
      const SizedBox(
        height: 10,
      ),
      Row(
        children: [
          HeatMapTile('hovered tile', 1, colorMap),
          const SizedBox(width: 5),
          const Text('1 to 5'),
        ],
      ),
      Row(
        children: [
          HeatMapTile('hovered tile', 5, colorMap),
          const SizedBox(width: 5),
          const Text('6 to 10'),
        ],
      ),
      Row(
        children: [
          HeatMapTile('hovered tile', 10, colorMap),
          const SizedBox(width: 5),
          const Text('11 to 19'),
        ],
      ),
      Row(
        children: [
          HeatMapTile('hovered tile', 20, colorMap),
          const SizedBox(width: 5),
          const Text('20 and above'),
        ],
      ),
    ];
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        child: SizedBox(
          width: 200,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: legend,
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildDayOfWeekLabels(BuildContext context) {
    return SizedBox(
        height: 7 * HeatMapTile.totalHeight,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: const [
            Text(
              'Mon',
              style: TextStyle(fontSize: HeatMapTile.height),
            ),
            Text(
              'Wed',
              style: TextStyle(fontSize: HeatMapTile.height),
            ),
            Text(
              'Sun',
              style: TextStyle(fontSize: HeatMapTile.height),
            ),
          ],
        ));
  }

  List<Positioned> _buildMonthLabels(List<DateTime> weeks) {
    final monthStartColumn = List.generate(12, (index) => -1);
    for (var i = 0; i < weeks.length; i++) {
      final week = weeks[i];
      final startMonth = week.month - 1;
      final endMonth = week.add(const Duration(days: 7)).month - 1;
      if (startMonth == 11 && endMonth == 0) {
        monthStartColumn[0] = 0;
        continue;
      }

      if (monthStartColumn[startMonth] < 0) {
        monthStartColumn[startMonth] = i;
      }

      if (monthStartColumn[endMonth] < 0) {
        monthStartColumn[endMonth] = i;
      }
    }

    final monthLabels = <Positioned>[];
    for (var i = 0; i < monthStartColumn.length; i++) {
      late String text;
      if (i == 0) {
        text = 'Jan';
      } else if (i == 1) {
        text = 'Feb';
      } else if (i == 2) {
        text = 'Mar';
      } else if (i == 3) {
        text = 'Apr';
      } else if (i == 4) {
        text = 'May';
      } else if (i == 5) {
        text = 'Jun';
      } else if (i == 6) {
        text = 'Jul';
      } else if (i == 7) {
        text = 'Aug';
      } else if (i == 8) {
        text = 'Sep';
      } else if (i == 9) {
        text = 'Oct';
      } else if (i == 10) {
        text = 'Nov';
      } else {
        text = 'Dec';
      }
      final label = Positioned(
          left: gridStart + monthStartColumn[i] * HeatMapTile.totalWidth,
          child: Text(text));
      monthLabels.add(label);
    }

    return monthLabels;
  }

  DateTime _firstHeatMapDay() {
    final firstDayOfYear = DateTime(year).weekday;
    final daysIntoPreviousCalendar = firstDayOfYear - 1;
    return DateTime(year).subtract(Duration(days: daysIntoPreviousCalendar));
  }
}
