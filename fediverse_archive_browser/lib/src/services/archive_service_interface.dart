import 'package:fediverse_archive_browser/src/services/path_mapper_service_interface.dart';
import 'package:result_monad/result_monad.dart';

import '../models/entry_tree_item.dart';
import '../models/local_image_archive_entry.dart';
import '../utils/exec_error.dart';
import 'connections_manager.dart';

class ArchiveService {
  ConnectionsManager get connectionsManager =>
      throw Exception('Not implemented');

  String get ownersName => throw Exception('Not implemented');

  PathMappingService get pathMappingService =>
      throw Exception('Not Implemented');

  void clearCaches() => throw Exception('Not implemented');

  FutureResult<List<EntryTreeItem>, ExecError> getPosts() async =>
      throw Exception('Not implemented');

  FutureResult<List<EntryTreeItem>, ExecError> getAllComments() =>
      throw Exception('Not implemented');

  FutureResult<List<EntryTreeItem>, ExecError> getOrphanedComments() =>
      throw Exception('Not implemented');

  Result<ImageEntry, ExecError> getImageByUrl(String url) =>
      throw Exception('Not implemented');
}
