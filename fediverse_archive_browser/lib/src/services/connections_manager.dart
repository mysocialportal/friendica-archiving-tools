import 'package:fediverse_archive_browser/src/models/connection.dart';
import 'package:result_monad/result_monad.dart';

class ConnectionsManager {
  final _connectionsById = <String, Connection>{};
  final _connectionsByName = <String, Connection>{};
  final _connectionsByProfileUrl = <Uri, Connection>{};

  int get length => _connectionsById.length;

  void clearCaches() {
    _connectionsById.clear();
    _connectionsByName.clear();
    _connectionsByProfileUrl.clear();
  }

  bool addConnection(Connection connection) {
    if (_connectionsById.containsKey(connection.id)) {
      return false;
    }
    _connectionsById[connection.id] = connection;
    _connectionsByName[connection.name] = connection;
    _connectionsByProfileUrl[connection.profileUrl] = connection;

    return true;
  }

  bool addAllConnections(Iterable<Connection> newConnections) {
    bool result = true;

    for (final connection in newConnections) {
      result &= addConnection(connection);
    }

    return result;
  }

  Result<Connection, String> getById(String id) {
    final result = _connectionsById[id];

    return result != null ? Result.ok(result) : Result.error('$id not found');
  }

  Result<Connection, String> getByName(String name) {
    final result = _connectionsByName[name];

    return result != null ? Result.ok(result) : Result.error('$name not found');
  }

  Result<Connection, String> getByProfileUrl(Uri url) {
    final result = _connectionsByProfileUrl[url];

    return result != null ? Result.ok(result) : Result.error('$url not found');
  }
}
