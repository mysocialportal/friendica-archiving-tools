import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fediverse_archive_browser/src/services/archive_service_provider.dart';

import 'screens/entries_screen.dart';
import 'screens/stats_screen.dart';
import 'settings/settings_controller.dart';
import 'settings/settings_view.dart';

class Home extends StatefulWidget {
  final SettingsController settingsController;
  final ArchiveServiceProvider archiveService;

  const Home(
      {Key? key,
      required this.settingsController,
      required this.archiveService})
      : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  static final Widget notInitialiedWidget = Container();
  final List<AppPageData> _pageData = [];
  final List<Widget> _pages = [];
  int _selectedIndex = 0;

  @override
  void initState() {
    _pageData.addAll([
      AppPageData(
          'Posts',
          Icons.home,
          () => EntriesScreen(
                populator: widget.archiveService.getPosts,
              )),
      AppPageData(
          'Orphan\nComments',
          Icons.comment,
          () => EntriesScreen(
                populator: widget.archiveService.getOrphanedComments,
              )),
      AppPageData('Stats', Icons.bar_chart, () => const StatsScreen()),
      AppPageData('Settings', Icons.settings, () => _buildSettingsView()),
    ]);
    for (var i = 0; i < _pageData.length; i++) {
      _pages.add(notInitialiedWidget);
    }

    if (Directory(widget.settingsController.rootFolder).existsSync()) {
      _setSelectedIndex(0);
    } else {
      _setSelectedIndex(_pageData.length - 1);
    }

    super.initState();
  }

  @override
  void dispose() {
    _pages.clear();
    super.dispose();
  }

  void _setSelectedIndex(int value) {
    setState(() {
      if (_pages[value] == notInitialiedWidget) {
        _pages[value] = _pageData[value].widget;
      }
      _selectedIndex = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        children: [
          _buildNavBar(),
          SizedBox(width: 1, child: Container(color: Colors.grey)),
          _buildMainArea(),
        ],
      ),
    );
  }

  Widget _buildNavBar() {
    return NavigationRail(
      destinations:
      _pageData.map((p) => p.navRailDestination).toList(),
      selectedIndex: _selectedIndex,
      onDestinationSelected: _setSelectedIndex,
      labelType: NavigationRailLabelType.all,
    );
  }

  Widget _buildMainArea() {
    return Expanded(
        child: IndexedStack(index: _selectedIndex, children: _pages));
  }

  Widget _buildSettingsView() {
    return SettingsView(controller: widget.settingsController);
  }
}

class AppPageData {
  final String label;
  final IconData icon;
  final Widget Function() _widgetBuilder;
  late final Widget widget = _widgetBuilder();
  final NavigationRailDestination navRailDestination;

  AppPageData(this.label, this.icon, widgetBuilder)
      : _widgetBuilder = widgetBuilder,
        navRailDestination = NavigationRailDestination(
            icon: Icon(icon),
            label: Text(
              label,
              textAlign: TextAlign.center,
            ));
}
