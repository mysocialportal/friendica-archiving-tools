import 'package:flutter/material.dart';

class FediverseArchiveBrowserTheme {
  static ThemeData dark = ThemeData.dark().copyWith(
    primaryColor: Colors.white,
  );

  static ThemeData light = ThemeData.light().copyWith(
    primaryColor: Colors.black,
  );

  static ThemeData darkroom = dark.copyWith(
    appBarTheme: const AppBarTheme(
      backgroundColor: Colors.black,
    ),
    scaffoldBackgroundColor: Colors.black,
    floatingActionButtonTheme: const FloatingActionButtonThemeData(
      foregroundColor: Colors.white,
      backgroundColor: Colors.indigo,
    ),
    bottomNavigationBarTheme: const BottomNavigationBarThemeData(
      selectedItemColor: Colors.green,
    ),
  );
}
