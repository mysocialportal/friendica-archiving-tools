import 'package:fediverse_archive_browser/src/mastodon/serializers/mastodon_media_attachment_serializer.dart';
import 'package:logging/logging.dart';

import '../../models/location_data.dart';
import '../../models/timeline_entry.dart';
import '../../utils/offsetdatetime_utils.dart';

final _logger = Logger('timelineEntryFromMastodonJson');
TimelineEntry timelineEntryFromMastodonJson(Map<String, dynamic> json) {
  final int timestamp = json.containsKey('published')
      ? OffsetDateTimeUtils.epochSecTimeFromTimeZoneString(json['published'])
          .fold(
              onSuccess: (value) => value,
              onError: (error) {
                _logger.severe("Couldn't read date time string: $error");
                return 0;
              })
      : 0;
  final id = json['id'] ?? '';
  final isReshare = json.containsKey('reblogged');
  final parentId = json['inReplyTo'] ?? '';
  final parentAuthor = json['in_reply_to_account_id'] ?? '';
  final parentAuthorId = json['in_reply_to_account_id'] ?? '';
  final body = json['content'] ?? '';
  final author = json['attributedTo'] ?? '';
  final authorId = json['attributedTo'] ?? '';
  const title = '';
  final externalLink = json['url'] ?? '';
  final actualLocationData = LocationData();
  final modificationTimestamp = timestamp;
  final backdatedTimestamp = timestamp;
  final mediaAttachments = (json['attachment'] as List<dynamic>? ?? [])
      .map((json) => mediaAttachmentfromMastodonJson(json))
      .toList();
  return TimelineEntry(
    creationTimestamp: timestamp,
    modificationTimestamp: modificationTimestamp,
    backdatedTimestamp: backdatedTimestamp,
    locationData: actualLocationData,
    body: body,
    isReshare: isReshare,
    id: id,
    parentId: parentId,
    parentAuthorId: parentAuthorId,
    externalLink: externalLink,
    author: author,
    authorId: authorId,
    parentAuthor: parentAuthor,
    title: title,
    links: [],
    mediaAttachments: mediaAttachments,
  );
}
