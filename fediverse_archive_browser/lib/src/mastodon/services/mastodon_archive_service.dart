import 'dart:convert';
import 'dart:io';

import 'package:path/path.dart' as p;
import 'package:result_monad/result_monad.dart';

import '../../models/entry_tree_item.dart';
import '../../models/local_image_archive_entry.dart';
import '../../services/archive_service_interface.dart';
import '../../services/connections_manager.dart';
import '../../utils/exec_error.dart';
import '../serializers/mastodon_timeline_entry_serializer.dart';
import 'mastodon_path_mapping_service.dart';

class MastodonArchiveService implements ArchiveService {
  @override
  final MastodonPathMappingService pathMappingService;

  final Map<String, ImageEntry> _imagesByRequestUrl = {};
  final List<EntryTreeItem> _postEntries = [];
  final List<EntryTreeItem> _orphanedCommentEntries = [];
  final List<EntryTreeItem> _allComments = [];
  @override
  final ConnectionsManager connectionsManager = ConnectionsManager();

  MastodonArchiveService({required this.pathMappingService});

  @override
  // TODO: implement ownersName
  String get ownersName => throw UnimplementedError();

  @override
  void clearCaches() {
    connectionsManager.clearCaches();
    _imagesByRequestUrl.clear();
    _orphanedCommentEntries.clear();
    _allComments.clear();
    _postEntries.clear();
  }

  @override
  FutureResult<List<EntryTreeItem>, ExecError> getPosts() async {
    if (_postEntries.isEmpty && _allComments.isEmpty) {
      _loadEntries();
    }

    return Result.ok(_postEntries);
  }

  @override
  FutureResult<List<EntryTreeItem>, ExecError> getAllComments() async {
    if (_postEntries.isEmpty && _allComments.isEmpty) {
      _loadEntries();
    }

    return Result.ok(_allComments);
  }

  @override
  FutureResult<List<EntryTreeItem>, ExecError> getOrphanedComments() async {
    if (_postEntries.isEmpty && _allComments.isEmpty) {
      _loadEntries();
    }

    return Result.ok(_orphanedCommentEntries);
  }

  @override
  Result<ImageEntry, ExecError> getImageByUrl(String url) {
    if (_imagesByRequestUrl.isEmpty) {
      _loadImages();
    }

    final result = _imagesByRequestUrl[url];
    return result == null
        ? Result.error(ExecError(errorMessage: '$url not found'))
        : Result.ok(result);
  }

  String get _baseArchiveFolder => pathMappingService.rootFolder;

  void _loadEntries() {
    final entriesJsonPath = p.join(_baseArchiveFolder, 'outbox.json');
    final jsonFile = File(entriesJsonPath);
    try {
      if (jsonFile.existsSync()) {
        final jsonText = jsonFile.readAsStringSync();
        final json = jsonDecode(jsonText) as Map<String, dynamic>;
        final entriesJson = json['orderedItems'] as List<dynamic>;
        final entries = entriesJson
            .where((e) => 'Create' == e['type'])
            .map((e) => e['object'])
            .map((e) => timelineEntryFromMastodonJson(e))
            .toList();
        final topLevelEntries =
            entries.where((element) => element.parentId.isEmpty);
        final commentEntries =
            entries.where((element) => element.parentId.isNotEmpty).toList();
        final entryTrees = <String, EntryTreeItem>{};

        final postTreeEntries = <EntryTreeItem>[];
        for (final entry in topLevelEntries) {
          final treeEntry = EntryTreeItem(entry, false);
          entryTrees[entry.id] = treeEntry;
          postTreeEntries.add(treeEntry);
        }

        final commentTreeEntries = <EntryTreeItem>[];
        commentEntries.sort(
            (c1, c2) => c1.creationTimestamp.compareTo(c2.creationTimestamp));
        for (final entry in commentEntries) {
          final parent = entryTrees[entry.parentId];
          final treeEntry = EntryTreeItem(entry, parent == null);
          parent?.addChild(treeEntry);
          entryTrees[entry.id] = treeEntry;
          commentTreeEntries.add(treeEntry);
        }

        _postEntries.clear();
        _postEntries.addAll(postTreeEntries);

        _allComments.clear();
        _allComments.addAll(commentTreeEntries);

        _orphanedCommentEntries.clear();
        _orphanedCommentEntries
            .addAll(entryTrees.values.where((element) => element.isOrphaned));
      }
    } catch (e) {
      print(e);
    }
  }

  void _loadImages() {
    final imageJsonPath = p.join(_baseArchiveFolder, 'images.json');
    final jsonFile = File(imageJsonPath);
    if (jsonFile.existsSync()) {
      final json = jsonDecode(jsonFile.readAsStringSync()) as List<dynamic>;
      final imageEntries = json.map((j) => ImageEntry.fromJson(j));
      for (final entry in imageEntries) {
        _imagesByRequestUrl[entry.url] = entry;
      }
    }
  }
}
