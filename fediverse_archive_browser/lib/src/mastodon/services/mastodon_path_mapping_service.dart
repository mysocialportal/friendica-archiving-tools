import 'dart:io';

import 'package:fediverse_archive_browser/src/settings/settings_controller.dart';
import 'package:logging/logging.dart';
import 'package:path/path.dart' as p;

import '../../services/path_mapper_service_interface.dart';

class MastodonPathMappingService implements PathMappingService {
  static final _logger = Logger('$MastodonPathMappingService');
  final SettingsController settings;
  final _archiveDirectories = <FileSystemEntity>[];

  MastodonPathMappingService(this.settings) {
    refresh();
  }

  String get rootFolder => settings.rootFolder;

  List<FileSystemEntity> get archiveDirectories =>
      List.unmodifiable(_archiveDirectories);

  void refresh() {
    _logger.fine('Refreshing path mapping service directory data.');
    if (!Directory(settings.rootFolder).existsSync()) {
      _logger.severe(
          "Base directory does not exist! can't do mapping of ${settings.rootFolder}");
      return;
    }
    _archiveDirectories.clear();

    final recursive = !_calcRootIsSingleArchiveFolder();
    _archiveDirectories.addAll(Directory(settings.rootFolder)
        .listSync(recursive: recursive)
        .where((element) =>
            element.statSync().type == FileSystemEntityType.directory &&
            p.basename(element.path) == 'media_attachments')
        .map((d) => d.parent));
  }

  String toFullPath(String relPath) {
    for (final file in _archiveDirectories) {
      final fullPath =
          p.join(file.path, relPath[0] == '/' ? relPath.substring(1) : relPath);
      if (File(fullPath).existsSync()) {
        return fullPath;
      }
    }

    _logger.fine(
        'Did not find a file with this relPath anywhere therefore returning the relPath');
    return relPath;
  }

  bool _calcRootIsSingleArchiveFolder() {
    for (final entity in Directory(rootFolder).listSync(recursive: false)) {
      if (_knownRootFilesAndFolders.contains(entity.uri.pathSegments
          .where((element) => element.isNotEmpty)
          .last)) {
        return true;
      }
    }

    return false;
  }

  static final _knownRootFilesAndFolders = [
    'media_attachments',
    'actor.json',
    'likes.json',
    'outbox.json',
  ];
}
