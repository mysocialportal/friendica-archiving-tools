import 'package:fediverse_archive_browser/src/utils/exec_error.dart';
import 'package:result_monad/result_monad.dart';
import 'package:time_machine/time_machine_text_patterns.dart';

class OffsetDateTimeUtils {
  static final _offsetTimeParser =
      OffsetDateTimePattern.createWithInvariantCulture(
          'ddd MMM dd HH:mm:ss o<+HHmm> yyyy');

  static Result<int, ExecError> epochSecTimeFromFriendicaString(
      String dateString) {
    final offsetDateTime = _offsetTimeParser.parse(dateString);
    if (!offsetDateTime.success) {
      return Result.error(ExecError.message(offsetDateTime.error.toString()));
    }

    return Result.ok(offsetDateTime.value.localDateTime
            .toDateTimeLocal()
            .millisecondsSinceEpoch ~/
        1000);
  }

  static Result<int, ExecError> epochSecTimeFromTimeZoneString(
      String dateString) {
    final offsetDateTime = OffsetDateTimePattern.generalIso.parse(dateString);
    if (!offsetDateTime.success) {
      return Result.error(ExecError.message(offsetDateTime.error.toString()));
    }

    return Result.ok(offsetDateTime.value.localDateTime
            .toDateTimeLocal()
            .millisecondsSinceEpoch ~/
        1000);
  }
}
