import 'dart:math';

import 'package:html/parser.dart' show parse;

class WordMapGenerator {
  final _words = <String, int>{};
  final int minimumWordSize;
  final Set<String> _filterWords;

  WordMapGenerator({Set<String>? filterWords, this.minimumWordSize = 1})
      : _filterWords = filterWords ?? <String>{};

  WordMapGenerator.withCommonWordsFilter({this.minimumWordSize = 1})
      : _filterWords = commonWords;

  void clear() {
    _words.clear();
  }

  void processEntry(String text) {
    final topLevelText = parse(text)
            .body
            ?.nodes
            .where((element) => element.nodeType == 3)
            .join(' ') ??
        text;

    final wordsFromText = topLevelText
        .toLowerCase()
        .replaceAll(RegExp(r'[^\w]+'), ' ')
        .replaceAll(RegExp(r'[_]+'), ' ')
        .split(RegExp(r'\s+'))
        .where((word) =>
            word.length >= minimumWordSize && !_filterWords.contains(word));
    for (final word in wordsFromText) {
      final oldCount = _words[word] ?? 0;
      _words[word] = oldCount + 1;
    }
  }

  List<WordMapItem> getTopList(int threshold) {
    if (_words.isEmpty) {
      return [];
    }

    final entries =
        _words.entries.map((e) => WordMapItem(e.key, e.value)).toList();
    entries.sort((e1, e2) => e2.count.compareTo(e1.count));
    return entries.getRange(0, min(entries.length, threshold)).toList();
  }
}

class WordMapItem {
  final String word;
  final int count;

  WordMapItem(this.word, this.count);

  @override
  String toString() {
    return 'WordMapItem{word: $word, count: $count}';
  }
}

const commonWords = {
  'does',
  'aren',
  'did',
  'the',
  'and',
  'for',
  'com',
  'you',
  'are',
  'www',
  'but',
  'not',
  'was',
  'all',
  'can',
  'out',
  'one',
  'how',
  'his',
  'him',
  'she',
  'her',
  'don',
  'has',
  'had',
  'why',
  'who',
  'too',
  'let',
  'may',
  'isn',
  'far',
  'utm',
  'yet',
  'that',
  'this',
  'http',
  'https',
  'html',
  'htm',
  'with',
  'they',
  'like',
  'from',
  'about',
  'just',
  'what',
  'their',
  'when',
  'will',
  'even',
  'there'
      'their',
  'than',
  'more',
  'them',
  'these',
  'been',
  'would',
  'there',
  'into',
  'only',
  'still',
  'which',
  'your',
  'have',
  'because',
  'much',
  'didn',
  'back',
  'were',
  'then',
  'very',
  'many'
      'maybe'
      'here',
  'ever',
  'doesn',
  'every',
  'having',
  'already',
  'some',
};
