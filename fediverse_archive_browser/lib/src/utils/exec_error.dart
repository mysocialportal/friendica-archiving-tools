import 'package:result_monad/result_monad.dart';

class ExecError {
  final int errorCode;
  final Object? exception;
  final String errorMessage;

  @override
  String toString() {
    return 'ExecError{\n  errorCode: $errorCode,\n  exception: $exception,\n  errorMessage: $errorMessage\n}';
  }

  ExecError({this.errorCode = -1, this.errorMessage = '', this.exception});

  ExecError.message(this.errorMessage)
      : errorCode = 0,
        exception = null;
}

extension ResultToExecError<T> on Result<T, dynamic> {
  Result<T, ExecError> mapExceptionErrorToExecError() =>
      mapError((error) => ExecError(exception: error));
}
