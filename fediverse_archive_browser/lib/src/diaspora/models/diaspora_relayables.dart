import 'package:fediverse_archive_browser/src/diaspora/models/diaspora_reaction.dart';
import 'package:fediverse_archive_browser/src/models/timeline_entry.dart';

class DiasporaRelayable {
  final DiasporaReaction? reaction;
  final TimelineEntry? comment;

  bool get isReaction => reaction != null;

  bool get isComment => comment != null;

  DiasporaRelayable({this.reaction, this.comment});
}
