class DiasporaReaction {
  final String authorString;
  final String guid;
  final String parentGuid;
  final ParentType parentType;
  final ReactionType reactionType;

  DiasporaReaction(
      {required this.authorString,
      this.guid = '',
      required this.parentGuid,
      this.parentType = ParentType.post,
      required this.reactionType});

  static DiasporaReaction fromJson(Map<String, dynamic> json) =>
      DiasporaReaction(
          authorString: json['author'] ?? '',
          guid: json['guid'] ?? '',
          parentGuid: json['parent_guid'] ?? '',
          parentType: _parentTypeFromString(json['parent_type'] ?? ''),
          reactionType: _reactionTypeFromBool(json['positive'] ?? true));
}

enum ParentType {
  unknown,
  comment,
  post,
}

ParentType _parentTypeFromString(String string) {
  final stringLower = string.toLowerCase();
  if (stringLower == 'post') {
    return ParentType.post;
  }

  if (stringLower == 'comment') {
    return ParentType.comment;
  }

  return ParentType.unknown;
}

enum ReactionType {
  unknown,
  dislike,
  like,
}

ReactionType _reactionTypeFromBool(bool value) {
  if (value) {
    return ReactionType.like;
  }

  return ReactionType.dislike;
}
