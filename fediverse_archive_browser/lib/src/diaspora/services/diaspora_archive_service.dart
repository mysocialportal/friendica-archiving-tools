import 'dart:io';

import 'package:fediverse_archive_browser/src/diaspora/services/diaspora_path_mapping_service.dart';
import 'package:fediverse_archive_browser/src/diaspora/services/diaspora_profile_json_reader.dart';
import 'package:fediverse_archive_browser/src/models/connection.dart';
import 'package:fediverse_archive_browser/src/services/archive_service_interface.dart';
import 'package:path/path.dart' as p;
import 'package:result_monad/result_monad.dart';

import '../../models/entry_tree_item.dart';
import '../../models/local_image_archive_entry.dart';
import '../../services/connections_manager.dart';
import '../../utils/exec_error.dart';
import '../models/diaspora_reaction.dart';
import '../serializers/diaspora_contact_serializer.dart';

class DiasporaArchiveService implements ArchiveService {
  @override
  final DiasporaPathMappingService pathMappingService;

  final Map<String, ImageEntry> _imagesByRequestUrl = {};
  final List<FileSystemEntity> _topLevelDirectories = [];
  final List<EntryTreeItem> _postEntries = [];
  final List<EntryTreeItem> _orphanedCommentEntries = [];
  final List<EntryTreeItem> _allComments = [];

  @override
  final ConnectionsManager connectionsManager = ConnectionsManager();
  String _ownersName = '';

  DiasporaArchiveService({required this.pathMappingService});

  String get ownersName => _ownersName;

  void clearCaches() {
    connectionsManager.clearCaches();
    _imagesByRequestUrl.clear();
    _orphanedCommentEntries.clear();
    _allComments.clear();
    _postEntries.clear();
    _topLevelDirectories.clear();
  }

  FutureResult<List<EntryTreeItem>, ExecError> getPosts() async {
    if (_postEntries.isEmpty && _allComments.isEmpty) {
      _loadProfileFile();
    }

    return Result.ok(_postEntries);
  }

  FutureResult<List<EntryTreeItem>, ExecError> getAllComments() async {
    if (_postEntries.isEmpty && _allComments.isEmpty) {
      _loadProfileFile();
    }

    return Result.ok(_allComments);
  }

  FutureResult<List<EntryTreeItem>, ExecError> getOrphanedComments() async {
    if (_postEntries.isEmpty && _allComments.isEmpty) {
      _loadProfileFile();
    }

    return Result.ok(_orphanedCommentEntries);
  }

  Result<ImageEntry, ExecError> getImageByUrl(String url) {
    if (_imagesByRequestUrl.isEmpty) {
      _populateTopLevelSubDirectory();
    }

    var result = _imagesByRequestUrl[url];
    if (result == null) {
      final localFile = _getLocalVersion(url);
      if (localFile.isSuccess) {
        _imagesByRequestUrl[url] = localFile.value;
        result = localFile.value;
      }
    }
    return result == null
        ? Result.error(ExecError(errorMessage: '$url not found'))
        : Result.ok(result);
  }

  String get _baseArchiveFolder => pathMappingService.rootFolder;

  void _loadProfileFile() {
    _ownersName = '';
    final archiveDir = Directory(_baseArchiveFolder);
    final jsonFiles = archiveDir.listSync().where((element) =>
        element.statSync().type == FileSystemEntityType.file &&
        element.path.toLowerCase().endsWith('json'));
    for (final file in jsonFiles) {
      final reader =
          DiasporaProfileJsonReader(file.absolute.path, connectionsManager);
      if (_ownersName.isEmpty) {
        _ownersName = reader.readOwnersName();
        reader.readContacts();
        final entryTree = <String, EntryTreeItem>{};
        final newPosts =
            reader.readPosts().map((e) => EntryTreeItem(e, false)).toList();

        for (final post in newPosts) {
          entryTree[post.id] = post;
        }

        final userComments = reader
            .readUserRelayables()
            .where((r) => r.isComment)
            .map((r) => r.comment!);
        final othersRelayables = reader.readOthersRelayables();
        final othersComments =
            othersRelayables.where((r) => r.isComment).map((r) => r.comment!);
        final othersReactions =
            othersRelayables.where((r) => r.isReaction).map((r) => r.reaction!);

        final allComments = [...userComments, ...othersComments];
        allComments.sort(
            (c1, c2) => c1.creationTimestamp.compareTo(c2.creationTimestamp));

        for (final comment in allComments) {
          final parentId = comment.parentId;
          final parent = entryTree[parentId];
          if (parent == null) {
            final newEntry = EntryTreeItem(comment, true);
            entryTree[comment.id] = newEntry;
            if (userComments.contains(comment)) {
              _orphanedCommentEntries.add(newEntry);
            }
          } else {
            parent.addChild(EntryTreeItem(comment, false));
          }
        }

        for (final reaction in othersReactions) {
          final treeEntry = entryTree[reaction.parentGuid];
          if (treeEntry == null) {
            continue;
          }

          final builtConnections = <String, Connection>{};
          final builtConnection = builtConnections[reaction.authorString] ??
              contactFromDiasporaId(reaction.authorString);
          builtConnections[reaction.authorString] = builtConnection;
          final result =
              connectionsManager.getByProfileUrl(builtConnection.profileUrl);
          final connection = result.fold(
              onSuccess: (c) => c,
              onError: (error) {
                connectionsManager.addConnection(builtConnection);
                return builtConnection;
              });

          switch (reaction.reactionType) {
            case ReactionType.unknown:
              break;
            case ReactionType.dislike:
              treeEntry.entry.dislikes.add(connection);
              break;
            case ReactionType.like:
              treeEntry.entry.likes.add(connection);
              break;
          }
        }

        _postEntries.addAll(newPosts);
      }
    }

    _postEntries.sort((p1, p2) => _reverseChronologicalSort(p1, p2));
    _orphanedCommentEntries.sort((p1, p2) => _reverseChronologicalSort(p1, p2));
  }

  int _reverseChronologicalSort(EntryTreeItem p1, EntryTreeItem p2) =>
      p2.entry.creationTimestamp.compareTo(p1.entry.creationTimestamp);

  void _populateTopLevelSubDirectory() {
    final topLevelDirectories = Directory(_baseArchiveFolder)
        .listSync(recursive: false)
        .where((e) => e.statSync().type == FileSystemEntityType.directory);
    _topLevelDirectories.addAll(topLevelDirectories);
  }

  Result<ImageEntry, ExecError> _getLocalVersion(String url) {
    final filename = Uri.parse(url).pathSegments.last;
    for (final dir in _topLevelDirectories) {
      final newPath = p.join(dir.path, filename);
      if (File(newPath).existsSync()) {
        final imageEntry =
            ImageEntry(postId: '', localFilename: newPath, url: url);
        return Result.ok(imageEntry);
      }
    }

    return Result.error(ExecError.message('Local file not found'));
  }
}
