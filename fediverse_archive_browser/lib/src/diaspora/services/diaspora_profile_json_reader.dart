import 'dart:convert';
import 'dart:io';

import 'package:fediverse_archive_browser/src/diaspora/models/diaspora_relayables.dart';
import 'package:fediverse_archive_browser/src/diaspora/serializers/diaspora_contact_serializer.dart';
import 'package:fediverse_archive_browser/src/diaspora/serializers/diaspora_relayables_serializer.dart';
import 'package:fediverse_archive_browser/src/models/connection.dart';
import 'package:fediverse_archive_browser/src/models/timeline_entry.dart';
import 'package:fediverse_archive_browser/src/services/connections_manager.dart';

import '../serializers/diaspora_posts_serializer.dart';

class DiasporaProfileJsonReader {
  final String jsonFilePath;
  final ConnectionsManager connectionsManager;
  final _jsonData = <String, dynamic>{};

  DiasporaProfileJsonReader(this.jsonFilePath, this.connectionsManager);

  Map<String, dynamic> get jsonData {
    if (_jsonData.isNotEmpty) {
      return _jsonData;
    }

    final jsonFile = File(jsonFilePath);
    if (jsonFile.existsSync()) {
      final json =
          jsonDecode(jsonFile.readAsStringSync()) as Map<String, dynamic>;
      _jsonData.addAll(json);
    }

    return _jsonData;
  }

  String readOwnersName() =>
      jsonData['user']?['profile']?['entity_data']?['author'] ?? 'Unknown';

  List<Connection> readContacts() {
    final json = jsonData;
    final userName = json['user']?['profile']?['entity_data']?['author'] ?? '';
    final userContact = Connection(name: userName, id: '0');
    connectionsManager.addConnection(userContact);
    final contactsJson = json['user']?['contacts'] as List<dynamic>;
    final contacts = contactsJson.map((j) => contactFromDiasporaJson(j));
    connectionsManager.addAllConnections(contacts);
    return contacts.toList();
  }

  List<TimelineEntry> readPosts() {
    if (connectionsManager.length == 0) {
      readContacts();
    }

    final json = jsonData;
    final postsJson = json['user']?['posts'] as List<dynamic>;
    final posts = postsJson
        .map((j) => timelineItemFromDiasporaPostJson(j, connectionsManager))
        .where((element) => element.isSuccess)
        .map((e) => e.value)
        .toList();

    posts
        .sort((p1, p2) => p2.creationTimestamp.compareTo(p1.creationTimestamp));
    return posts;
  }

  List<DiasporaRelayable> readUserRelayables() {
    if (connectionsManager.length == 0) {
      readContacts();
    }

    return _readRelayableJson(jsonData['user']?['relayables'] ?? []);
  }

  List<DiasporaRelayable> readOthersRelayables() {
    if (connectionsManager.length == 0) {
      readContacts();
    }

    return _readRelayableJson(jsonData['others_data']?['relayables'] ?? []);
  }

  List<DiasporaRelayable> _readRelayableJson(List<dynamic> relayableJsonList) =>
      relayableJsonList
          .map((e) => relayableFromDiasporaPostJson(e))
          .where((r) => r.isSuccess)
          .map((r) => r.value)
          .toList();
}
