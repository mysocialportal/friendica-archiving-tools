import 'package:fediverse_archive_browser/src/diaspora/models/diaspora_relayables.dart';
import 'package:fediverse_archive_browser/src/models/timeline_entry.dart';
import 'package:fediverse_archive_browser/src/utils/exec_error.dart';
import 'package:fediverse_archive_browser/src/utils/offsetdatetime_utils.dart';
import 'package:logging/logging.dart';
import 'package:markdown/markdown.dart';
import 'package:result_monad/result_monad.dart';

import '../models/diaspora_reaction.dart';

final _logger = Logger('DiasporaPostsSerializer');
const _commentType = 'comment';
const _likeType = 'like';
const _knownRelayableTypes = [_commentType, _likeType];

Result<DiasporaRelayable, ExecError> relayableFromDiasporaPostJson(
    Map<String, dynamic> json) {
  if (!json.containsKey('entity_data')) {
    return Result.error(ExecError.message('Relayable item has no entity data'));
  }
  final entityType = json['entity_type'] ?? '';
  final entityData = json['entity_data'] ?? {};
  if (!_knownRelayableTypes.contains(entityType)) {
    final guid = entityData['guid'];
    final error = 'Unknown entity type $entityType for Relayable ID: $guid';
    _logger.severe(error);
    return Result.error(ExecError.message(error));
  }

  if (entityType == _commentType) {
    return _buildCommentRelayable(entityData);
  }

  if (entityType == _likeType) {
    return _buildReactionRelayable(entityData);
  }

  return Result.error(ExecError.message('Unknown type: $entityType'));
}

Result<DiasporaRelayable, ExecError> _buildCommentRelayable(
    Map<String, dynamic> entityData) {
  final author = entityData['author'] ?? '';
  final guid = entityData['guid'] ?? '';
  final parentGuid = entityData['parent_guid'] ?? '';
  final commentMarkdown = entityData['text'] ?? '';
  final commentHtml = markdownToHtml(commentMarkdown);
  final epochTime = OffsetDateTimeUtils.epochSecTimeFromTimeZoneString(
          entityData['created_at'] ?? '')
      .getValueOrElse(() => -1);

  final timelineEntry = TimelineEntry(
    id: guid,
    creationTimestamp: epochTime,
    body: commentHtml,
    author: author,
    parentId: parentGuid,
    externalLink: _buildCommentUrl(author, parentGuid, guid),
  );
  return Result.ok(DiasporaRelayable(comment: timelineEntry));
}

Result<DiasporaRelayable, ExecError> _buildReactionRelayable(
    Map<String, dynamic> entityData) {
  final reaction = DiasporaReaction.fromJson(entityData);
  return Result.ok(DiasporaRelayable(reaction: reaction));
}

String _buildCommentUrl(String author, String parentGuid, String commentGuid) {
  final accountIdPieces = author.split('@');
  if (accountIdPieces.length != 2) {
    return commentGuid;
  }

  final server = accountIdPieces[1];

  return 'https://$server/p/$parentGuid#$commentGuid';
}
