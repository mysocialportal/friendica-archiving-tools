# Friendica Archive Browser Changelog

## Version 1.0.0 

Initial version

### New Features
* Posts and comments browsing/filtering (including media and links)
* Statistics view of posts/commetsn (filterable) including:
  * Posts/comments per year, month of year, day of week
  * Annual Heatmap of posts/comments
  * Top interactors with ability to click to see profiles
  * Top words
