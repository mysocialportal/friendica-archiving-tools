extension ListEqualityTest<T> on List<T> {
  bool equals(List<T> list2) {
    if (length != list2.length) {
      return false;
    }

    for (var i = 0; i < length; i++) {
      if (this[i] != list2[i]) {
        return false;
      }
    }

    return true;
  }
}
