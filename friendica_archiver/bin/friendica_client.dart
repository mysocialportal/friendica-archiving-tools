import 'dart:convert';
import 'dart:io';

import 'package:result_monad/result_monad.dart';

import 'exec_error.dart';
import 'models.dart';

class FriendicaClient {
  final String username;
  final String password;
  final String serverName;
  late final String _authHeader;

  FriendicaClient(
      {required this.username,
      required this.password,
      required this.serverName}) {
    final authenticationString = "$username:$password";
    final encodedAuthString = base64Encode(utf8.encode(authenticationString));
    _authHeader = "Basic $encodedAuthString";
  }

  FutureResult<List<FriendicaEntry>, ExecError> getTimeline(
      String userId, int page, int count) async {
    final request = Uri.parse(
        'https://$serverName/api/statuses/user_timeline?screen_name=$userId&count=$count&page=$page');
    return (await _getApiRequest(request)).mapValue((postsJson) => postsJson
        .map((postJson) => FriendicaEntry.fromJson(postJson))
        .toList());
  }

  FutureResult<HttpClientResponse, ExecError> getUrl(Uri url) async {
    try {
      final request = await HttpClient().getUrl(url);
      request.headers.add('authorization', _authHeader);
      final response = await request.close();
      return Result.ok(response);
    } catch (e) {
      return Result.error(
          ExecError(type: ErrorType.localError, message: e.toString()));
    }
  }

  FutureResult<List<dynamic>, ExecError> _getApiRequest(Uri url) async {
    final responseResult = await getUrl(url);
    if (responseResult.isFailure) {
      return responseResult.mapValue((value) => <dynamic>[]);
    }
    final body = await responseResult.value.transform(utf8.decoder).join('');
    final bodyJson = jsonDecode(body) as List<dynamic>;
    return Result.ok(bodyJson);
  }
}
