class ExecError {
  final ErrorType type;
  final String message;

  ExecError({required this.type, this.message = ''});

  @override
  String toString() {
    return 'ExecError{type: $type, message: $message}';
  }
}

enum ErrorType {
  authentication,
  localError,
  missingEndpoint,
}
