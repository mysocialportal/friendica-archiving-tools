# Friendica Archive Browser Changelog

## Version 1.2.0

Minor version update to fix a broken REST service call for pulling the timeline.

## Version 1.1.0

Minor version update to increase reporting during run

### Features
* Specify when an existing post/comment is being updated.

## Version 1.0.0 

Initial version

### Features
* Login using username/email and password
* Specify page size and number of pages to pull
* Specify which page to restart from
* Archives posts, comments, and images associated with them
* Ability to restart from an existing archive file
